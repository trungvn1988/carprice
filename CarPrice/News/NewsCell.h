//
//  NewsCell.h
//  CarPrice
//
//  Created by Trung 1988 on 4/25/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell

@property (nonnull, retain) IBOutlet UILabel* lblTitle;
@property (nonnull, retain) IBOutlet UIImageView* imgDesc;
@property (nonnull, retain) NSDictionary* currentDic;

@end
