//
//  NewsVC.m
//  CarPrice
//
//  Created by Trung 1988 on 4/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "NewsVC.h"

@interface NewsVC ()

@end

@implementation NewsVC

@synthesize arrFeeds;

@synthesize item, titleFeed, link, element, imgLink;

@synthesize tblFeed;

@synthesize loadedAd, bannerView, adView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Bản tin";
    arrFeeds = [NSMutableArray new];
    
    [tblFeed registerNib:[UINib nibWithNibName:@"NewsCell" bundle:nil] forCellReuseIdentifier:@"NewsCell"];
    
    [(AppDelegate*)[UIApplication sharedApplication].delegate showAd:nil target:self];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        loadedAd = FALSE;
        [self loadAd];
    }];
}

-(void) viewWillAppear:(BOOL)animated{
    [[NSOperationQueue new] addOperationWithBlock:^{
        NSXMLParser* parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://autopro.com.vn/tin-tuc.rss"]];
        parser.delegate = self;
        
        [parser setShouldResolveExternalEntities:NO];
        [parser parse];
    }];
    
    [self setColorTitle:[UIColor whiteColor]];
    
    if(!loadedAd && arrFeeds.count > 0)
        [self loadedAd];
}

-(void) loadAd{
    self.bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 60)];
    self.bannerView.adUnitID = @"ca-app-pub-2022191528301116/3723412583";
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    GADRequest* request = [GADRequest request];
    
    [self.bannerView loadRequest:request];
}

//Google Ads
-(void) adViewDidReceiveAd:(GADBannerView *)bannerView{
    loadedAd = TRUE;
    [tblFeed reloadData];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerView = nil;
    adView = [[FBAdView alloc] initWithPlacementID:@"1765263163791610_1777642235887036"
                                            adSize:kFBAdSizeHeight50Banner
                                rootViewController:self];
    adView.delegate = self;
    [adView loadAd];
}

//Facebook Ads
- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error{
    NSLog(@"error %@", error);
}

- (void)adViewDidLoad:(FBAdView *)adView;{
    loadedAd = TRUE;
    [tblFeed reloadData];
}

-(void) adViewDidClick:(FBAdView *)adView{
     NSLog(@"Banner ad was clicked.");
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    NSLog(@"Banner ad did finish click handling.");
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    NSLog(@"Banner ad impression is being captured.");
}


-(void) parserDidStartDocument:(NSXMLParser *)parser{
    NSLog(@"START %@", parser);
    [arrFeeds removeAllObjects];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
       [self.view addLoadingView];
    }];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = [[NSMutableString alloc] init];
    [element appendString:elementName];
    
    if ([elementName isEqualToString:@"item"]) {
        item    = [[NSMutableDictionary alloc] init];
        titleFeed   = [[NSMutableString alloc] init];
        link    = [[NSMutableString alloc] init];
        imgLink = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([element isEqualToString:@"title"]) {
        titleFeed = [[NSMutableString alloc] init];
        [titleFeed appendString:string];
    }
    
    if ([element isEqualToString:@"link"]) {
        link = [[NSMutableString alloc] init];
        [link appendString:string];
    }
    
    if([element isEqualToString:@"description"]){
        imgLink = [[NSMutableString alloc] init];
        
        @try {
            NSInteger startIndex = [string rangeOfString:@"<img src="].location + @"<img src=".length + 1;
            NSInteger endIndex = [string rangeOfString:@"width="].location;
            
            string = [string substringWithRange:NSMakeRange(startIndex, endIndex - startIndex)];
            string = [string stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [imgLink appendString:string];
        } @catch (NSException *exception) {
            NSLog(@"%@", exception);
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:titleFeed forKey:@"title"];
        [item setObject:link forKey:@"link"];
        [item setObject:imgLink forKey:@"imgLink"];
        
        [arrFeeds addObject:[item copy]];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.view removeLoadingView];
        [tblFeed reloadData];
    }];
    NSLog(@"counter %li", arrFeeds.count);
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(loadedAd)
        return arrFeeds.count + 1;
    return arrFeeds.count;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"NewsCell";
    if(indexPath.row == 0){
        NewsCell *cell = [tblFeed dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (!cell) {
            cell = [NewsCell new];
        }
        
        NSDictionary* dicContent = [arrFeeds objectAtIndex:indexPath.row];
        cell.lblTitle.text = [dicContent objectForKey:@"title"];
        cell.currentDic = dicContent;
        [cell.imgDesc sd_setImageWithURL:[NSURL URLWithString:[dicContent objectForKey:@"imgLink"]]];
        
        return cell;
    }
    
    if(loadedAd && indexPath.row == 1){
        UITableViewCell* cell = [UITableViewCell new];
        if(bannerView){
            [cell.contentView addSubview:bannerView];
        }else{
            [cell.contentView addSubview:adView];
        }
        
        return cell;
    }
    
    NSInteger cellIndex = indexPath.row;
    if(loadedAd){
        cellIndex = cellIndex - 1;
    }
    
    NewsCell *cell = [tblFeed dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (!cell) {
        cell = [NewsCell new];
    }
    
    NSDictionary* dicContent = [arrFeeds objectAtIndex:cellIndex];
    cell.currentDic = dicContent;
    cell.lblTitle.text = [dicContent objectForKey:@"title"];
    [cell.imgDesc sd_setImageWithURL:[NSURL URLWithString:[dicContent objectForKey:@"imgLink"]]];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NewsCell* cell = [tblFeed cellForRowAtIndexPath:indexPath];
    
    if(![cell isKindOfClass:[NewsCell class]])
        return;
    if(!cell.currentDic)
        return;
    
    NewsDetail* newsDetail = [NewsDetail new];
    newsDetail.hidesBottomBarWhenPushed = YES;
    newsDetail.dicContent = cell.currentDic;
    
    [self.navigationController pushViewController:newsDetail animated:YES];
    
}

@end
