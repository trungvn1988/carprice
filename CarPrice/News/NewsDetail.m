//
//  NewsDetail.m
//  CarPrice
//
//  Created by Trung 1988 on 4/25/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "NewsDetail.h"

@interface NewsDetail ()

@end

@implementation NewsDetail

@synthesize dicContent;
@synthesize webView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"dicContent %@", dicContent);
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    self.title = [dicContent objectForKey:@"title"];
    NSString* link = [dicContent objectForKey:@"link"];
    
    link = [link stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    link = [link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:link]]];
}

-(void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"error %@", error);
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) webViewDidStartLoad:(UIWebView *)webView{
    
}

-(void) webViewDidFinishLoad:(UIWebView *)webView{
    
}

@end
