//
//  NewsVC.h
//  CarPrice
//
//  Created by Trung 1988 on 4/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsCell.h"
#import "NewsDetail.h"

@interface NewsVC : UIViewController <NSXMLParserDelegate, UITableViewDelegate, UITableViewDataSource, FBAdViewDelegate, GADBannerViewDelegate>

@property (nonnull, retain) IBOutlet UITableView* tblFeed;

@property (nonnull, retain) NSMutableArray* arrFeeds;

@property (nonnull, retain) NSMutableString* element;
@property (nonnull, retain) NSMutableDictionary* item;
@property (nonnull, retain) NSMutableString* titleFeed;
@property (nonnull, retain) NSMutableString* link;
@property (nonnull, retain) NSMutableString* imgLink;

@property (nonnull, retain) FBAdView *adView;
@property (nullable, retain) IBOutlet GADBannerView  *bannerView;
@property BOOL loadedAd;

@end
