//
//  NewsCell.m
//  CarPrice
//
//  Created by Trung 1988 on 4/25/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "NewsCell.h"

@implementation NewsCell

@synthesize lblTitle;
@synthesize imgDesc;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
