//
//  NewsDetail.h
//  CarPrice
//
//  Created by Trung 1988 on 4/25/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetail : UIViewController <UIWebViewDelegate>

@property (nonnull, retain) NSDictionary* dicContent;

@property (nonnull, retain) IBOutlet UIWebView* webView;

@end
