//
//  ShowRoomCell.h
//  CarPrice
//
//  Created by Trung 1988 on 3/6/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowRoomCell : UITableViewCell

@property (nonnull, retain) NSDictionary* dicShowRoom;

@property (nonnull, retain) IBOutlet UIImageView* imgShowRoom;
@property (nonnull, retain) IBOutlet UILabel* lblAddress;
@property (nonnull, retain) IBOutlet UILabel* lblCity;
@property (nonnull, retain) IBOutlet UILabel* lblName;
@property (nonnull, retain) IBOutlet UILabel* lblPhoneNo;

-(void) drawCell;

@end
