//
//  ShowRoomView.m
//  CarPrice
//
//  Created by Trung 1988 on 2/9/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "ShowRoomView.h"

@interface ShowRoomView ()

@end

@implementation ShowRoomView

@synthesize ref;
@synthesize dicShowRoom, arrShowRoom, arrShowRoomFull, tblShowRoom;
@synthesize searchBar;
@synthesize loadedAd, adView, bannerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Showroom";
    [self setColorTitle:[UIColor whiteColor]];
    
    [self loadShowRoom];
    
    arrShowRoomFull = [NSMutableArray new];
    arrShowRoom = [NSMutableArray new];
    
    tblShowRoom.tableFooterView = [UIView new];
    tblShowRoom.estimatedRowHeight = 50.0;
    tblShowRoom.rowHeight = UITableViewAutomaticDimension;
    
    [tblShowRoom registerNib:[UINib nibWithNibName:@"ShowRoomCell" bundle:nil] forCellReuseIdentifier:@"ShowRoomCell"];
    
    loadedAd = FALSE;
}

-(void) viewWillAppear:(BOOL)animated{
    if(!loadedAd && arrShowRoom.count > 0)
        [self loadAd];
}

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"call me %@", searchText);
    
    [arrShowRoom removeAllObjects];
    
    if(searchText.length < 2){
        [arrShowRoom addObjectsFromArray:arrShowRoomFull];
        [tblShowRoom reloadData];
        return;
    }
    
    for (NSDictionary* dicContent in arrShowRoomFull) {
        NSLog(@"dicContent %@", dicContent);
        NSArray* allValue = [dicContent allValues];
        for (NSString* content in allValue) {
            if ([[content uppercaseString] containsString:[searchText uppercaseString]]) {
                [arrShowRoom addObject:dicContent];
                break;
            }
        }
    }
    
    [tblShowRoom reloadData];
}

-(void) loadAd{
    self.bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 50)];
    self.bannerView.adUnitID = @"ca-app-pub-2022191528301116/3723412583";
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    GADRequest* request = [GADRequest request];
    [self.bannerView loadRequest:request];
}

//Google Ads
-(void) adViewDidReceiveAd:(GADBannerView *)bannerView{
    loadedAd = TRUE;
    [tblShowRoom reloadData];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerView = nil;
    adView = [[FBAdView alloc] initWithPlacementID:@"1765263163791610_1777642235887036"
                                            adSize:kFBAdSizeHeight50Banner
                                rootViewController:self];
    adView.delegate = self;
    [adView loadAd];
}

//Facebook Ads
- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error{
    NSLog(@"error %@", error);
}

- (void)adViewDidLoad:(FBAdView *)adView;{
    loadedAd = TRUE;
    [tblShowRoom reloadData];
}

-(void) adViewDidClick:(FBAdView *)adView{
    NSLog(@"Banner ad was clicked.");
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    NSLog(@"Banner ad did finish click handling.");
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    NSLog(@"Banner ad impression is being captured.");
}

-(void) loadShowRoom{
    [[UIApplication sharedApplication].windows.firstObject addLoadingView];
    
    ref = [[[FIRDatabase database] reference] child:@"Showroom"];
    [ref observeSingleEventOfType:FIRDataEventTypeValue
                        withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                            [[UIApplication sharedApplication].windows.firstObject removeLoadingView];
                            if(!snapshot.value)
                                return;
                            
                            dicShowRoom = snapshot.value;
                            
                            NSArray *keys = [dicShowRoom allKeys];
                            keys = [keys sortedArrayUsingComparator:^(id a, id b) {
                                return [a compare:b options:NSNumericSearch];
                            }];
                            
                            for (NSString* key in keys) {
                                [arrShowRoom addObject:[dicShowRoom objectForKey:key]];
                                [arrShowRoomFull addObject:[dicShowRoom objectForKey:key]];
                            }
                            
                            [tblShowRoom reloadData];
                            
                            NSLog(@"%@", [arrShowRoom firstObject]);
                            
                            [self loadAd];
                            
                        } withCancelBlock:^(NSError * _Nonnull error) {
                            NSLog(@"error %@", error);
                            [[UIApplication sharedApplication].windows.firstObject removeLoadingView];
                        }];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(loadedAd)
        return arrShowRoom.count + 1;
    return arrShowRoom.count;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"ShowRoomCell";
    
    @try {
        if(indexPath.row == 0){
            ShowRoomCell *cell = [tblShowRoom dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (!cell) {
                cell = [ShowRoomCell new];
            }
            
            cell.dicShowRoom = [arrShowRoom objectAtIndex:indexPath.row];
            [cell drawCell];
            return cell;
        }
        
        if(loadedAd && indexPath.row == 1){
            UITableViewCell* cell = [UITableViewCell new];
            if(bannerView){
                [cell.contentView addSubview:bannerView];
            }else{
                [cell.contentView addSubview:adView];
            }
            
            return cell;
        }
        
        NSInteger cellIndex = indexPath.row;
        if(loadedAd){
            cellIndex = cellIndex - 1;
        }
        
        ShowRoomCell *cell = [tblShowRoom dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (!cell) {
            cell = [ShowRoomCell new];
        }
        
        cell.dicShowRoom = [arrShowRoom objectAtIndex:cellIndex];
        [cell drawCell];
        
        return cell;
        
    } @catch (NSException *exception) {
        return [UITableViewCell new];
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [searchBar resignFirstResponder];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSInteger cellIndex = indexPath.row;
        if(loadedAd){
            cellIndex = cellIndex - 1;
        }
        if(cellIndex < 0)
            cellIndex = 0;
        ShowRoomCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        
        UIAlertController* alertCall = [UIAlertController alertControllerWithTitle:@"Gọi cho Showroom" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertCall addAction:[UIAlertAction actionWithTitle:@"Bỏ qua" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [alertCall addAction:[UIAlertAction actionWithTitle:@"Đồng ý" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString* phoneNo = [cell.dicShowRoom objectForKey:@"phoneNo"];
            if(phoneNo.length > 0){
                NSString* telephone = [NSString stringWithFormat:@"tel:%@", phoneNo];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telephone]];
            }
        }]];
        
        [self presentViewController:alertCall animated:YES completion:nil];
    }];
}

@end
