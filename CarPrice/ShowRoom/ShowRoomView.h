//
//  ShowRoomView.h
//  CarPrice
//
//  Created by Trung 1988 on 2/9/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowRoomCell.h"

@interface ShowRoomView : UIViewController <UITableViewDelegate, UITableViewDataSource, FBAdViewDelegate, UISearchBarDelegate, GADBannerViewDelegate>

@property (nonnull, retain) NSDictionary* dicShowRoom;
@property (nonnull, retain) NSMutableArray* arrShowRoom;
@property (nonnull, retain) NSMutableArray* arrShowRoomFull;

@property (nullable, nonatomic) FIRDatabaseReference *ref;

@property (nonnull, retain) IBOutlet UITableView* tblShowRoom;

@property (nonnull, retain) IBOutlet UISearchBar* searchBar;

@property (nonnull, retain) FBAdView *adView;
@property (nullable, retain) IBOutlet GADBannerView  *bannerView;
@property BOOL loadedAd;

@end
