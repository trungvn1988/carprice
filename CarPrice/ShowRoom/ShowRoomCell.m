//
//  ShowRoomCell.m
//  CarPrice
//
//  Created by Trung 1988 on 3/6/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "ShowRoomCell.h"

@implementation ShowRoomCell

@synthesize dicShowRoom;
@synthesize imgShowRoom;
@synthesize lblCity, lblName, lblAddress, lblPhoneNo;

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void) drawCell{
    NSString* urlLink = [dicShowRoom objectForKey:@"imgURL"];
    [imgShowRoom sd_setImageWithURL:[NSURL URLWithString:urlLink] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
    }];
    
    lblPhoneNo.text = [dicShowRoom objectForKey:@"phoneNo"];
    lblAddress.text = [dicShowRoom objectForKey:@"address"];
    lblName.text = [dicShowRoom objectForKey:@"name"];
    lblCity.text = [dicShowRoom objectForKey:@"city"];
}

@end
