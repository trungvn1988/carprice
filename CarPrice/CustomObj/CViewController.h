//
//  CViewController.h
//  SingleToMingle
//
//  Created by Van Ngoc Trung on 10/23/15.
//  Copyright © 2015 TrungVN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Customize) <UIGestureRecognizerDelegate>

-(void) setColorTitle:(UIColor*) color;

-(void) optimizeEdge;

@end
