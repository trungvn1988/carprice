//
//  CNavigationController.m
//  KaraokeHoney
//
//  Created by Van Ngoc Trung on 10/22/15.
//  Copyright © 2015 Honey Studio. All rights reserved.
//

#import "CNavigationController.h"

@implementation UINavigationController (Customize)

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationBar] setShadowImage:[UIImage new]];
}

@end
