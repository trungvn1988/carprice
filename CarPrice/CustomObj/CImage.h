//
//  CImage.h
//  SingleToMingle
//
//  Created by Trung 1988 on 6/10/16.
//  Copyright © 2016 pehsys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>

@interface UIImage (Customize)

-(BOOL) detectFace;

-(UIImage*) blurImage:(CGFloat) blurScale;

+(UIImage *)imageFromColor:(UIColor *)color;

-(UIImage *)scaleImagetoSize:(CGSize)newSize;

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;

- (UIImage *)fixOrientation;

@end
