//
//  CNavigationController.h
//  KaraokeHoney
//
//  Created by Van Ngoc Trung on 10/22/15.
//  Copyright © 2015 Honey Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Customize)

@end
