//
//  CString.m
//  Market
//
//  Created by Trung 1988 on 12/22/16.
//  Copyright © 2016 pehsys@gmail.com. All rights reserved.
//

#import "CString.h"

@implementation NSString (Extension)

+ (NSString*) decodeHtmlUnicodeCharactersToString:(NSString*)str
{
    NSMutableString* string = [[NSMutableString alloc] initWithString:str];  // #&39; replace with '
    NSString* unicodeStr = nil;
    NSString* replaceStr = nil;
    int counter = -1;
    
    for(int i = 0; i < [string length]; ++i)
    {
        unichar char1 = [string characterAtIndex:i];
        for (int k = i + 1; k < [string length] - 1; ++k)
        {
            unichar char2 = [string characterAtIndex:k];
            
            if (char1 == '&'  && char2 == '#')
            {
                ++counter;
                unicodeStr = [string substringWithRange:NSMakeRange(i + 2 , 2)];
                // read integer value i.e, 39
                replaceStr = [string substringWithRange:NSMakeRange (i, 5)];     //     #&39;
                [string replaceCharactersInRange: [string rangeOfString:replaceStr] withString:[NSString stringWithFormat:@"%c",[unicodeStr intValue]]];
                break;
            }
        }
    }
    
    if (counter > 1)
        return  [self decodeHtmlUnicodeCharactersToString:string];
    else
        return string;
}

- (NSString *)subStringToLength:(NSInteger)length {
    NSString *returnString = @"";
    if ([self length] > length) {
        returnString = [NSString stringWithFormat:@"%@..", [self substringToIndex:length]];
    } else {
        returnString = self;
    }
    return returnString;
}

-(NSString*) convertHTMLString{
    NSString* strResult = [self stringByReplacingOccurrencesOfString:@">amp;" withString:@"&"];
    strResult = [strResult stringByReplacingOccurrencesOfString:@">quot;" withString:@"\""];
    return strResult;
}

@end
