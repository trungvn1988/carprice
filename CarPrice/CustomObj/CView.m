//
//  CView.m
//  KT
//
//  Created by Trung 1988 on 5/5/16.
//  Copyright © 2016 AnPSoft. All rights reserved.
//
#import "CView.h"
@implementation UIView (Customize)
-(CGFloat) x{
    return self.frame.origin.x;
}
-(CGFloat) y{
    return self.frame.origin.y;
}
-(CGFloat) width{
    return self.frame.size.width;
}
-(CGFloat) height{
    return self.frame.size.height;
}
-(CGFloat) bottomEdge{
    return self.frame.origin.y + self.frame.size.height;
}
-(CGFloat) rightEdge{
    return self.frame.origin.x + self.frame.size.width;
}
-(void) changeX:(CGFloat) x{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}
-(void) changeY:(CGFloat) y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}
-(void) changeWidth:(CGFloat) width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}
-(void) changeHeight:(CGFloat) height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}
-(void) changeOrigin:(CGFloat) x
                   y:(CGFloat) y{
    CGRect frame = self.frame;
    frame.origin.x = x;
    frame.origin.y = y;
    self.frame = frame;
}
-(void) changeSize:(CGFloat) width
            height:(CGFloat) height{
    CGRect frame = self.frame;
    frame.size.width = width;
    frame.size.height = height;
    self.frame = frame;
}
-(void) animateX:(CGFloat) x{
    [UIView animateWithDuration:0.25 animations:^{
        [self changeX:x];
    }];
}
-(void) animateY:(CGFloat) y{
    [UIView animateWithDuration:0.25 animations:^{
        [self changeY:y];
    }];
}
-(void) animateWidth:(CGFloat) width{
    [UIView animateWithDuration:0.25 animations:^{
        [self changeWidth:width];
    }];
}
-(void) animateHeight:(CGFloat) height{
    [UIView animateWithDuration:0.25 animations:^{
        [self changeHeight:height];
    }];
}
-(void) animateOrigin:(CGFloat) x
                    y:(CGFloat) y{
    [UIView animateWithDuration:0.25 animations:^{
        [self changeOrigin:x y:y];
    }];
}
-(void) animateSize:(CGFloat) width
             height:(CGFloat) height{
    [UIView animateWithDuration:0.25 animations:^{
        [self changeSize:width height:height];
    }];
}
-(void)shakeView {
    CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"position"];
    [shake setDuration:0.1];
    [shake setRepeatCount:2];
    [shake setAutoreverses:YES];
    [shake setFromValue:[NSValue valueWithCGPoint:
                         CGPointMake(self.center.x - 5,self.center.y)]];
    [shake setToValue:[NSValue valueWithCGPoint:
                       CGPointMake(self.center.x + 5, self.center.y)]];
    [self.layer addAnimation:shake forKey:@"position"];
}
-(void) setBorder:(CGFloat) borderWidth
            color:(UIColor*) borderColor{
    self.layer.borderWidth = borderWidth;
    self.layer.borderColor = borderColor.CGColor;
    self.layer.masksToBounds = YES;
}

- (void)addLoadingView {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
        hud.label.text = @"Đang tải...";
    }];
}

- (void)removeLoadingView {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [MBProgressHUD hideHUDForView:self animated:YES];
    }];
}

@end
