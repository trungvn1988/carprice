//
//  CObject.h
//  Market
//
//  Created by Trung 1988 on 12/14/16.
//  Copyright © 2016 pehsys@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CImage.h"
#import "CViewController.h"
#import "CNavigationController.h"
#import "CBarButtonItem.h"
#import "CColor.h"
#import "CView.h"
#import "CButton.h"
#import "CString.h"
#import "CTextField.h"

#define NO_OPEN_CATE 1988

@interface NSObject (Extension)

- (void)startNetworkActivityIndicator;
- (void)stopNetworkActivityIndicator;

-(void) writeFile:(id) object
          withKey:(NSString*) key;

-(id) getFileWithKey:(NSString*) key;

@end
