//
//  CColor.h
//  SingleToMingle
//
//  Created by Trung 1988 on 6/23/16.
//  Copyright © 2016 pehsys. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UIColor (Customize)
+(UIColor*) hexColor:(NSString*) hex;
@end
