//
//  CViewController.m
//  SingleToMingle
//
//  Created by Van Ngoc Trung on 10/23/15.
//  Copyright © 2015 TrungVN. All rights reserved.
//

#import "CViewController.h"
#import "CColor.h"
#import "CImage.h"

@implementation UIViewController (Customize)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

-(void) viewDidLoad{
    [self optimizeEdge];
}

-(void) viewWillAppear:(BOOL)animated{
    [self setColorTitle:[UIColor whiteColor]];
}

- (void)viewDidAppear:(BOOL)animated {
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

#pragma clang diagnostic pop

-(void) setColorTitle:(UIColor*) color{
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:color}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageFromColor:[UIColor hexColor:@"#363636"]] forBarMetrics:UIBarMetricsDefault];
}

-(void) optimizeEdge{
    if([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

@end
