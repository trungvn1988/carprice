//
//  CView.h
//  KT
//
//  Created by Trung 1988 on 5/5/16.
//  Copyright © 2016 AnPSoft. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface UIView (Customize)
/*
 @ Author Trung
 @ Get the property of view in shorter way.
 */
-(CGFloat) x;
-(CGFloat) y;
-(CGFloat) width;
-(CGFloat) height;
-(CGFloat) bottomEdge;
-(CGFloat) rightEdge;
/*
 @ Author Trung
 @ Change value of view without animation.
 */
-(void) changeX:(CGFloat) x;
-(void) changeY:(CGFloat) y;
-(void) changeWidth:(CGFloat) width;
-(void) changeHeight:(CGFloat) height;
-(void) changeOrigin:(CGFloat) x
                   y:(CGFloat) y;
-(void) changeSize:(CGFloat) width
            height:(CGFloat) height;
/*
 @ Author Trung
 @ Change value of view with animation.
 */
-(void) animateX:(CGFloat) x;
-(void) animateY:(CGFloat) y;
-(void) animateWidth:(CGFloat) width;
-(void) animateHeight:(CGFloat) height;
-(void) animateOrigin:(CGFloat) x
                   y:(CGFloat) y;
-(void) animateSize:(CGFloat) width
            height:(CGFloat) height;
-(void)shakeView;
-(void) setBorder:(CGFloat) borderWidth
            color:(UIColor*) borderColor;

- (void)addLoadingView;
- (void)removeLoadingView;

@end
