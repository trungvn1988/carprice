//
//  CObject.m
//  Market
//
//  Created by Trung 1988 on 12/14/16.
//  Copyright © 2016 pehsys@gmail.com. All rights reserved.
//

#import "CObject.h"

@implementation NSObject (Extension)

- (void)startNetworkActivityIndicator {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)stopNetworkActivityIndicator {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


-(void) writeFile:(id) object
          withKey:(NSString*) key{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(id) getFileWithKey:(NSString*) key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

@end
