//
//  CString.h
//  Market
//
//  Created by Trung 1988 on 12/22/16.
//  Copyright © 2016 pehsys@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

+ (NSString*) decodeHtmlUnicodeCharactersToString:(NSString*)str;

- (NSString *)subStringToLength:(NSInteger)length;

-(NSString*) convertHTMLString;

@end
