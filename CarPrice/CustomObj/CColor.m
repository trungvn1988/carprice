//
//  CColor.m
//  SingleToMingle
//
//  Created by Trung 1988 on 6/23/16.
//  Copyright © 2016 pehsys. All rights reserved.
//
#import "CColor.h"
@implementation UIColor (Customize)
+(UIColor*) hexColor:(NSString*) hex{
    int red = 0;
    int green = 0;
    int blue = 0;
    sscanf([hex UTF8String], "#%02X%02X%02X", &red, &green, &blue);
    return  [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1.0];
}
@end
