//
//  BrandList.h
//  CarPrice
//
//  Created by Trung 1988 on 2/6/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrandListCell.h"
#import "CarDetail.h"

@interface BrandList : UIViewController <UITableViewDelegate, UITableViewDataSource, FBAdViewDelegate, GADBannerViewDelegate>

@property (nonnull, retain) NSDictionary* dicBrand;
@property (nonnull, retain) IBOutlet UITableView* tblMain;
@property (nonnull, retain) NSMutableArray* arrCar;

@property (nonnull, retain) FBAdView *adView;
@property (nullable, retain) IBOutlet GADBannerView  *bannerView;

@property BOOL loadedAd;

-(void) sortContent;

@end
