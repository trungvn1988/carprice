//
//  BrandListCell.h
//  CarPrice
//
//  Created by Trung 1988 on 2/6/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandListCell : UITableViewCell

@property (nonnull, retain) NSDictionary* dicCar;

@property (nonnull, retain) IBOutlet UIImageView* imgMain;
@property (nonnull, retain) IBOutlet UILabel* lblName;
@property (nonnull, retain) IBOutlet UILabel* lblPrice;
@property (nonnull, retain) IBOutlet UILabel* lblType;
@property (nonnull, retain) IBOutlet UILabel* lblOrigin;
@property (nonnull, retain) IBOutlet UILabel* lblEngine;

-(void) drawCell;

@end
