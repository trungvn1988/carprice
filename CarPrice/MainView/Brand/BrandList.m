//
//  BrandList.m
//  CarPrice
//
//  Created by Trung 1988 on 2/6/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "BrandList.h"

@interface BrandList ()

@end

@implementation BrandList

@synthesize dicBrand, tblMain, arrCar;

@synthesize adView, loadedAd, bannerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(!arrCar){
        arrCar = [NSMutableArray new];
    }
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem createRightButtonImage:[UIImage imageNamed:@"sort-icon"]
                                                                            Target:self
                                                                            Action:@selector(sortContent)];
    
    tblMain.estimatedRowHeight = 50;
    tblMain.rowHeight = UITableViewAutomaticDimension;
    
    [tblMain registerNib:[UINib nibWithNibName:@"BrandListCell" bundle:nil] forCellReuseIdentifier:@"BrandListCell"];
    
    [self reloadContent];
    loadedAd = FALSE;
    [self loadAd];
}

-(void) loadAd{
    self.bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 50)];
    self.bannerView.adUnitID = @"ca-app-pub-2022191528301116/3723412583";
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    GADRequest* request = [GADRequest request];
    [self.bannerView loadRequest:request];
}

//Google Ads
-(void) adViewDidReceiveAd:(GADBannerView *)bannerView{
    loadedAd = TRUE;
    [tblMain reloadData];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerView = nil;
    adView = [[FBAdView alloc] initWithPlacementID:@"1765263163791610_1777642235887036"
                                            adSize:kFBAdSizeHeight50Banner
                                rootViewController:self];
    adView.delegate = self;
    [adView loadAd];
}

//Facebook Ads
- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error{
    NSLog(@"error %@", error);
}

- (void)adViewDidLoad:(FBAdView *)adView;{
    loadedAd = TRUE;
    [tblMain reloadData];
}

-(void) adViewDidClick:(FBAdView *)adView{
    NSLog(@"Banner ad was clicked.");
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    NSLog(@"Banner ad did finish click handling.");
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    NSLog(@"Banner ad impression is being captured.");
}

-(void) reloadContent{
    if(!dicBrand)
        return;
    
    self.title = [NSString stringWithFormat:@"%@ - %@", [dicBrand objectForKey:@"name"], [dicBrand objectForKey:@"trademark"]];
    
    [self sortType];
}

-(void) sortType{
    NSInteger type = [[[NSUserDefaults standardUserDefaults] objectForKey:@"sorttype"] integerValue];
    NSDictionary* dicCar = [dicBrand objectForKey:@"car"];
    if(![dicCar isKindOfClass:[NSDictionary class]])
        return;
    switch (type) {
        case 1:{
            NSArray* arrKey = [dicCar keysSortedByValueUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                NSNumber* price1 = [NSNumber numberWithInteger:[[[(NSDictionary*)obj1 objectForKey:@"price"] stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue]];
                NSNumber* price2 = [NSNumber numberWithInteger:[[[(NSDictionary*)obj2 objectForKey:@"price"] stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue]];
                
                return [price2 compare:price1];
            }];
            
            [arrCar removeAllObjects];
            for (NSString* key in arrKey){
                [arrCar addObject:[dicCar objectForKey:key]];
            }
            
            [tblMain reloadData];
        }
            break;
        case 2:{
            NSArray* arrKey = [dicCar keysSortedByValueUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                NSNumber* price1 = [NSNumber numberWithInteger:[[[(NSDictionary*)obj1 objectForKey:@"price"] stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue]];
                NSNumber* price2 = [NSNumber numberWithInteger:[[[(NSDictionary*)obj2 objectForKey:@"price"] stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue]];
                
                return [price1 compare:price2];
            }];
            
            [arrCar removeAllObjects];
            for (NSString* key in arrKey){
                [arrCar addObject:[dicCar objectForKey:key]];
            }
            
            [tblMain reloadData];
        }
            break;
        case 3:{
            NSArray* arrKey = [dicCar keysSortedByValueUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                NSNumber* power1 = [NSNumber numberWithInteger:[[(NSDictionary*)obj1 objectForKey:@"power"] integerValue]];
                NSNumber* power2 = [NSNumber numberWithInteger:[[(NSDictionary*)obj2 objectForKey:@"power"] integerValue]];
                return [power2 compare:power1];
            }];
            
            [arrCar removeAllObjects];
            for (NSString* key in arrKey){
                [arrCar addObject:[dicCar objectForKey:key]];
            }
            
            [tblMain reloadData];
        }
            break;
        case 4:{
            NSArray* arrKey = [dicCar keysSortedByValueUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [[(NSDictionary*)obj1 objectForKey:@"name"] compare:[(NSDictionary*)obj2 objectForKey:@"name"]];
            }];
            [arrCar removeAllObjects];
            for (NSString* key in arrKey){
                [arrCar addObject:[dicCar objectForKey:key]];
            }
            [tblMain reloadData];
        }
            break;
            
        default:
            break;
    }
}

-(void) sortContent{
    
    BOOL showAd = [(AppDelegate*)[UIApplication sharedApplication].delegate showAd:@selector(sortContent)
                                                                            target:self];
    if(!showAd){
        UIAlertController* alertVC = [UIAlertController alertControllerWithTitle:nil message:@"Sắp xếp" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alertVC addAction:[UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleCancel handler:nil]];
        [alertVC addAction:[UIAlertAction actionWithTitle:@"Giá cả (Cao tới thấp)" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"sorttype"];
            [self sortType];
        }]];
        
        [alertVC addAction:[UIAlertAction actionWithTitle:@"Giá cả (Thấp tới cao)" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"sorttype"];
            [self sortType];
        }]];
        
        [alertVC addAction:[UIAlertAction actionWithTitle:@"Công suất lớn" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"sorttype"];
            [self sortType];
        }]];
        
        [alertVC addAction:[UIAlertAction actionWithTitle:@"Tên xe" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"sorttype"];
            [self sortType];
        }]];
        
        [self presentViewController:alertVC animated:YES completion:nil];
    }
}

-(void) backView{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(loadedAd)
        return arrCar.count + 1;
    return arrCar.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"BrandListCell";
    
    if(indexPath.row == 0){
        BrandListCell *cell = [tblMain dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (!cell) {
            cell = [BrandListCell new];
        }
        
        cell.dicCar = [arrCar objectAtIndex:indexPath.row];
        [cell drawCell];
        return cell;
    }
    
    if(loadedAd && indexPath.row == 1){
        UITableViewCell* cell = [UITableViewCell new];
        if(bannerView){
            [cell.contentView addSubview:bannerView];
        }
        else{
            [cell.contentView addSubview:adView];
        }
        
        return cell;
    }
    
    NSInteger cellIndex = indexPath.row;
    if(loadedAd){
        cellIndex = cellIndex - 1;
    }
    
    BrandListCell *cell = [tblMain dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (!cell) {
        cell = [BrandListCell new];
    }
    
    cell.dicCar = [arrCar objectAtIndex:cellIndex];
    [cell drawCell];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger cellIndex = indexPath.row;
    if(loadedAd){
        cellIndex = cellIndex - 1;
    }
    if(cellIndex < 0)
        cellIndex = 0;
    BrandListCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    if(![cell isKindOfClass:[BrandListCell class]])
        return;
    CarDetail* carDetail = [CarDetail new];
    carDetail.carDetail = cell.dicCar;
    [self.navigationController pushViewController:carDetail animated:YES];
}

@end
