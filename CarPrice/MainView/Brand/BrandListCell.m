//
//  BrandListCell.m
//  CarPrice
//
//  Created by Trung 1988 on 2/6/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "BrandListCell.h"

@implementation BrandListCell

@synthesize lblName, imgMain, lblPrice, lblType, lblOrigin, lblEngine;

@synthesize dicCar;

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void) drawCell{
    if(!dicCar)
        return;
    lblName.text = [dicCar objectForKey:@"name"];
    lblPrice.text = [NSString stringWithFormat:@"%@ VND", [dicCar objectForKey:@"price"]];
    lblType.text = [dicCar objectForKey:@"type"];
    lblOrigin.text = [dicCar objectForKey:@"origin"];
    lblEngine.text = [dicCar objectForKey:@"engine"];
    
    NSString* mainURL = [[dicCar objectForKey:@"image"] objectForKey:@"main"];
    
    if(mainURL.length > 0){
        [imgMain sd_setImageWithURL:[NSURL URLWithString:mainURL]
         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
             if(!image){
                 NSString* subURL = [[dicCar objectForKey:@"image"] objectForKey:@"sub"];
                 [imgMain sd_setImageWithURL:[NSURL URLWithString:subURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                     if(!image){
                         [imgMain setImage:[UIImage imageNamed:@"no-image"]];
                     }
                 }];

             }
         }];
    }
}

@end
