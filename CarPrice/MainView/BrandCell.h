//
//  BrandCell.h
//  CarPrice
//
//  Created by Trung 1988 on 2/3/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandCell : UICollectionViewCell

@property (nonnull, retain) NSDictionary* dicBrand;
@property (nonnull, retain) IBOutlet UIImageView* imgLogo;
@property (nonnull, retain) IBOutlet UILabel* lblTradeMark;

-(void) drawCell;

@end
