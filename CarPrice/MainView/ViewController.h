//
//  ViewController.h
//  CarPrice
//
//  Created by Trung 1988 on 2/2/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <FirebaseAuth/FirebaseAuth.h>
#import "BrandList.h"
#import "SearchView.h"

@interface ViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, FBInterstitialAdDelegate>

@property (nonnull, retain) IBOutlet UICollectionView* brandCollection;

@property (nullable, nonatomic) FIRDatabaseReference *ref;

@property (nonnull, retain) NSMutableArray* arrBrand;
@property (nonnull, retain) NSDictionary* dicBrand;

@property (nonnull, retain) NSDate* requestDate;

-(void) loadDataBase;

@end

