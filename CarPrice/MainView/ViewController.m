//
//  ViewController.m
//  CarPrice
//
//  Created by Trung 1988 on 2/2/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "ViewController.h"
#import "BrandCell.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize ref;
@synthesize arrBrand, dicBrand;
@synthesize brandCollection;
@synthesize requestDate;

- (void)viewDidLoad {
    [super viewDidLoad];
    if(!arrBrand){
        arrBrand = [NSMutableArray new];
    }
    self.title = @"Thương Hiệu";
    [self setColorTitle:[UIColor whiteColor]];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem createRightButtonImage:[UIImage imageNamed:@"search-icon"]
                                                                              Target:self
                                                                              Action:@selector(showSearchView)];
}

-(void) showSearchView{
    BOOL showAd = [(AppDelegate*)[UIApplication sharedApplication].delegate showAd:@selector(showSearchView)
                                                                            target:self];
    if(!showAd){
        SearchView* searchView = [SearchView new];
        searchView.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:searchView animated:YES];
    }
}

-(void) loadDataBase{
    if(!dicBrand)
        [[UIApplication sharedApplication].windows.firstObject addLoadingView];
    
    ref = [[[FIRDatabase database] reference] child:@"Brand"];
    [ref observeSingleEventOfType:FIRDataEventTypeValue
                        withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                            [[UIApplication sharedApplication].windows.firstObject removeLoadingView];
                            if(!snapshot.value)
                                return;
                            
                            dicBrand = snapshot.value;
                            [self writeFile:dicBrand withKey:@"DATABASE"];
                            requestDate = [NSDate date];
                            [self reloadDataContent];
                            
                        } withCancelBlock:^(NSError * _Nonnull error) {
                            NSLog(@"error %@", error);
                            [[UIApplication sharedApplication].windows.firstObject removeLoadingView];
                        }];
}

-(void) viewWillAppear:(BOOL)animated{
    dicBrand = [self getFileWithKey:@"DATABASE"];
    if(!requestDate){
        [[NSOperationQueue new] addOperationWithBlock:^{
            [self loadDataBase];
        }];
        [self reloadDataContent];
        return;
    }
    
    NSTimeInterval gap = [[NSDate date] timeIntervalSinceDate:requestDate];
    if(gap > 12*60*60) { //12 hours
        [[NSOperationQueue new] addOperationWithBlock:^{
            [self loadDataBase];
        }];
        [self reloadDataContent];
        return;
    }
    
    [self reloadDataContent];
}

-(void) reloadDataContent{
    [[NSOperationQueue new] addOperationWithBlock:^{
        [arrBrand removeAllObjects];
        NSArray *keys = [dicBrand allKeys];
        keys = [keys sortedArrayUsingComparator:^(id a, id b) {
            return [a compare:b options:NSNumericSearch];
        }];
        
        for (NSString* key in keys) {
            [arrBrand addObject:[dicBrand objectForKey:key]];
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
           [brandCollection reloadData];
        }];
    }];
}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrBrand.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BrandCell* cell = (BrandCell*)[brandCollection dequeueReusableCellWithReuseIdentifier:@"BrandCell" forIndexPath:indexPath];
    NSArray *keys = [dicBrand allKeys];
    keys = [keys sortedArrayUsingComparator:^(id a, id b) {
        return [a compare:b options:NSNumericSearch];
    }];
    
    NSDictionary* brand = [arrBrand objectAtIndex:indexPath.row];
    if(brand){
        cell.dicBrand = brand;
        cell.lblTradeMark.text = [keys objectAtIndex:indexPath.row];
        [cell drawCell];
    }
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    BrandCell* cell = (BrandCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if(cell.dicBrand){
        BrandList* brandList = [BrandList new];
        brandList.dicBrand = cell.dicBrand;
        brandList.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:brandList animated:YES];
    }
}

@end
