//
//  SearchView.h
//  CarPrice
//
//  Created by Trung 1988 on 2/10/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrandListCell.h"
#import "CarDetail.h"

@interface SearchView : UIViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonnull, retain) IBOutlet UISearchBar* searchBar;
@property (nonnull, retain) IBOutlet UITableView* tblResult;

@property (nonnull, retain) NSMutableArray* arrCar;

@property (nonnull, retain) NSDictionary* dicBrand;

@end
