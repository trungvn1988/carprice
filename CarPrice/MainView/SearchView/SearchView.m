//
//  SearchView.m
//  CarPrice
//
//  Created by Trung 1988 on 2/10/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "SearchView.h"

@interface SearchView ()

@end

@implementation SearchView

@synthesize searchBar;
@synthesize tblResult;

@synthesize arrCar;
@synthesize dicBrand;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Tìm kiếm";
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    
    UITapGestureRecognizer* tapSearch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endSearch)];
    tapSearch.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapSearch];
    
    arrCar = [NSMutableArray new];
    dicBrand = [self getFileWithKey:@"DATABASE"];
    
    tblResult.tableFooterView = [UIView new];
    [tblResult registerNib:[UINib nibWithNibName:@"BrandListCell" bundle:nil] forCellReuseIdentifier:@"BrandListCell"];
    tblResult.rowHeight = UITableViewAutomaticDimension;
    tblResult.estimatedRowHeight = 44;
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar2{
    [searchBar resignFirstResponder];
}

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [arrCar removeAllObjects];
    if (searchText.length < 3) {
        [tblResult reloadData];
        return;
    }
    
    NSMutableArray* keySearch = [[searchText componentsSeparatedByString:@" "] mutableCopy];
    for (NSString* searchTag in [keySearch mutableCopy]) {
        if(searchTag.length < 3)
            [keySearch removeObject:searchTag];
    }
    
    if (keySearch.count == 0) {
        [tblResult reloadData];
        return;
    }
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        [arrCar removeAllObjects];
        for (NSString* key in [dicBrand allKeys]) {
            NSDictionary* dicCars = [[dicBrand objectForKey:key] objectForKey:@"car"];
            for (NSString* keyCar in [dicCars allKeys]) {
                NSDictionary* car = [dicCars objectForKey:keyCar];
                for (NSString* searchValue in [car allValues]) {
                    if(![searchValue isKindOfClass:[NSString class]])
                        continue;
                    
                    for(NSString* searchKey in keySearch) {
                        if ([[searchValue uppercaseString] containsString:[searchKey uppercaseString]] && ![arrCar containsObject:car]) {
                            [arrCar addObject:car];
                            continue;
                        }
                    }
                }
            }
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [tblResult reloadData];
        }];
    }];
}

-(void) endSearch{
    [searchBar resignFirstResponder];
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrCar.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"BrandListCell";
    
    BrandListCell *cell = [tblResult dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (!cell) {
        cell = [BrandListCell new];
    }
    
    cell.dicCar = [arrCar objectAtIndex:indexPath.row];
    [cell drawCell];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BrandListCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    CarDetail* carDetail = [CarDetail new];
    carDetail.carDetail = cell.dicCar;
    [self.navigationController pushViewController:carDetail animated:YES];
}

@end
