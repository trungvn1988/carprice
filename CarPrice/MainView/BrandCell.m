//
//  BrandCell.m
//  CarPrice
//
//  Created by Trung 1988 on 2/3/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "BrandCell.h"

@implementation BrandCell

@synthesize dicBrand;
@synthesize imgLogo, lblTradeMark;

-(void) drawCell{
    NSString* logoURL = [dicBrand objectForKey:@"logo"];
    [imgLogo sd_setImageWithURL:[NSURL URLWithString:logoURL]
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                          if(!image){
                              [imgLogo setImage:[UIImage imageNamed:@"no-image"]];
                          }
    }];
}

@end
