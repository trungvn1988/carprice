//
//  ReviewCar.h
//  CarPrice
//
//  Created by Trung 1988 on 4/28/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewCar : UIViewController <UIWebViewDelegate>


@property (nonnull, retain) IBOutlet UIWebView *reviewWeb;
@property (nonnull, retain) NSString* urlReview;

@end
