//
//  CarDetail.h
//  CarPrice
//
//  Created by Trung 1988 on 2/7/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountCost.h"
#import "ReviewCar.h"


@interface CarDetail : UIViewController <UITableViewDelegate, UITableViewDataSource, FBAdViewDelegate, GADBannerViewDelegate>

@property (nonnull, retain) NSDictionary* carDetail;
@property (nonnull, retain) IBOutlet UIScrollView* scrlDetail;
@property (nonnull, retain) IBOutlet UITableView* tblDetail;

@property NSInteger currentIndex;
@property (nonnull, retain) NSTimer* autoTimer;

@property (nonnull, retain) IBOutlet UIButton* btnReview;
@property (nonnull, retain) IBOutlet NSLayoutConstraint* btnReviewHeight;
@property (nonnull, retain) IBOutlet UIView* adViewAll;
@property (nonnull, retain) IBOutlet NSLayoutConstraint* adViewHeight;

@property (nonnull, retain) FBAdView *adView;
@property (nonnull, retain) IBOutlet GADBannerView  *bannerView;
@property BOOL loadedAd;

@property (nonnull, retain) IBOutlet UIButton* btnLike;

-(IBAction) likeCar;

-(IBAction) reviewCar;

@end
