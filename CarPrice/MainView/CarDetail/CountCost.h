//
//  CountCost.h
//  CarPrice
//
//  Created by Trung 1988 on 2/8/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountCost : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonnull, retain) NSDictionary* dicDetail;
@property (nonnull, retain) IBOutlet UILabel* lblCost;
@property (nonnull, retain) IBOutlet UITextField* txtArea;

@property (nonnull, retain) IBOutlet UITableView* tblCount;

@property (nonnull, retain) IBOutlet UIPickerView* provincePicker;
@property (nonnull, retain) IBOutlet NSLayoutConstraint* provincePickerBottom;
@property (nonnull, retain) NSMutableArray* arrProvince;

@property NSInteger priceDeal;
@property NSInteger taxNo;
@property NSInteger roadTax;
@property NSInteger insuranceTax;
@property NSInteger lisenceTax;
@property NSInteger lisenceFee;

@end
