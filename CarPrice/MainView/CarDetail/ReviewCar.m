//
//  ReviewCar.m
//  CarPrice
//
//  Created by Trung 1988 on 4/28/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "ReviewCar.h"

@interface ReviewCar ()

@end

@implementation ReviewCar

@synthesize reviewWeb, urlReview;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"urlReview %@", urlReview);
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    
    self.title = @"Đánh giá xe";
    
    if(urlReview.length > 0){
        NSURL* url = [NSURL URLWithString:urlReview];
        [reviewWeb loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) webViewDidStartLoad:(UIWebView *)webView{
}

-(void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
}

-(void) webViewDidFinishLoad:(UIWebView *)webView{
}

@end
