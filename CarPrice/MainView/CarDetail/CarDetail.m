//
//  CarDetail.m
//  CarPrice
//
//  Created by Trung 1988 on 2/7/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "CarDetail.h"

@interface CarDetail ()

@end

@implementation CarDetail

@synthesize carDetail;
@synthesize scrlDetail;
@synthesize tblDetail;
@synthesize autoTimer;

@synthesize currentIndex;

@synthesize btnReview;
@synthesize btnReviewHeight;
@synthesize adViewAll;
@synthesize adViewHeight;

@synthesize loadedAd, bannerView, adView;

@synthesize btnLike;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem createRightButtonImage:[UIImage imageNamed:@"cost-icon"]
                                                                            Target:self
                                                                            Action:@selector(countCost)];
    
    self.title = [carDetail objectForKey:@"name"];
    [self drawImage];
    tblDetail.tableFooterView = [UIView new];
    
    currentIndex = 0;
    
    UITapGestureRecognizer* tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(destroyTimer)];
    [scrlDetail addGestureRecognizer:tapScroll];
    
    
    NSLog(@"carDetail %@", carDetail);
    
    NSString* reviewLink = [carDetail objectForKey:@"reviewLink"];
    if(reviewLink.length == 0){
        btnReviewHeight.constant = 0;
    }
    
    adViewHeight.constant = 0.0;
    
    [self loadAd];
}

-(void) loadAd{
    self.bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 50)];
    self.bannerView.adUnitID = @"ca-app-pub-2022191528301116/3723412583";
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    GADRequest* request = [GADRequest request];
    [self.bannerView loadRequest:request];
}

//Google Ads
-(void) adViewDidReceiveAd:(GADBannerView *)bannerView2{
    loadedAd = TRUE;
    [adViewAll addSubview:bannerView];
    adViewHeight.constant = 50.0;
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    adView = [[FBAdView alloc] initWithPlacementID:@"1765263163791610_1777642235887036"
                                            adSize:kFBAdSizeHeight50Banner
                                rootViewController:self];
    adView.delegate = self;
    [adView loadAd];
}

//Facebook Ads
- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error{
    NSLog(@"error %@", error);
}

- (void)adViewDidLoad:(FBAdView *)adView2{
    loadedAd = TRUE;
    [adViewAll addSubview:adView];
    adViewHeight.constant = 50.0;
}

-(void) adViewDidClick:(FBAdView *)adView{
    NSLog(@"Banner ad was clicked.");
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    NSLog(@"Banner ad did finish click handling.");
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    NSLog(@"Banner ad impression is being captured.");
}

-(IBAction) reviewCar{
    ReviewCar* reviewCar = [ReviewCar new];
    reviewCar.urlReview = [carDetail objectForKey:@"reviewLink"];
    [self.navigationController pushViewController:reviewCar animated:YES];
}

-(void) destroyTimer{
    [autoTimer invalidate];
    autoTimer = nil;
}

-(void) viewWillAppear:(BOOL)animated{
    if(autoTimer){
        [autoTimer invalidate];
        autoTimer = nil;
    }
    
    autoTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        [self autoScroll];
    }];
    
    NSArray* arrLike = [self getFileWithKey:@"FAV_CAR"];
    btnLike.selected = [arrLike containsObject:self.carDetail];
}

-(void) viewDidDisappear:(BOOL)animated{
    [autoTimer invalidate];
    autoTimer = nil;
}

-(void) autoScroll{
    currentIndex++;
    if(currentIndex == 4){
        currentIndex = 0;
    }
    
    [scrlDetail setContentOffset:CGPointMake(currentIndex*scrlDetail.width, 1) animated:YES];
}

-(void) drawImage{
    NSDictionary* dicImage = [carDetail objectForKey:@"image"];
    NSString* strURLMain = [dicImage objectForKey:@"main"];
    NSString* strURLSub = [dicImage objectForKey:@"sub"];
    NSString* strURLSub2 = [dicImage objectForKey:@"sub2"];
    NSString* strURLSub3 = [dicImage objectForKey:@"sub3"];
    
    if(strURLMain.length > 0){
        UIImageView* imgMain = [[UIImageView alloc] initWithFrame:scrlDetail.frame];
        imgMain.contentMode = UIViewContentModeScaleAspectFit;
        [imgMain sd_setImageWithURL:[NSURL URLWithString:strURLMain]
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              if(!image){
                                  [imgMain setImage:[UIImage imageNamed:@"no-image"]];
                              }
                          }];
        [scrlDetail addSubview:imgMain];
    }
    
    if(strURLSub.length > 0){
        UIImageView* imgMain = [[UIImageView alloc] initWithFrame:scrlDetail.frame];
        imgMain.contentMode = UIViewContentModeScaleAspectFit;
        [imgMain sd_setImageWithURL:[NSURL URLWithString:strURLSub]
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              if(!image){
                                  [imgMain setImage:[UIImage imageNamed:@"no-image"]];
                              }
                          }];
        [imgMain changeX:scrlDetail.width];
        [scrlDetail addSubview:imgMain];
        [scrlDetail setContentSize:CGSizeMake(imgMain.rightEdge, scrlDetail.height)];
    }
    
    if(strURLSub2.length > 0){
        UIImageView* imgMain = [[UIImageView alloc] initWithFrame:scrlDetail.frame];
        imgMain.contentMode = UIViewContentModeScaleAspectFit;
        [imgMain sd_setImageWithURL:[NSURL URLWithString:strURLSub2]
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              if(!image){
                                  [imgMain setImage:[UIImage imageNamed:@"no-image"]];
                              }
                          }];
        [imgMain changeX:scrlDetail.width*2];
        [scrlDetail addSubview:imgMain];
        [scrlDetail setContentSize:CGSizeMake(imgMain.rightEdge, scrlDetail.height)];
    }
    
    if(strURLSub3.length > 0){
        UIImageView* imgMain = [[UIImageView alloc] initWithFrame:scrlDetail.frame];
        imgMain.contentMode = UIViewContentModeScaleAspectFit;
        [imgMain sd_setImageWithURL:[NSURL URLWithString:strURLSub3]
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              if(!image){
                                  [imgMain setImage:[UIImage imageNamed:@"no-image"]];
                              }
                          }];
        [imgMain changeX:scrlDetail.width*3];
        [scrlDetail addSubview:imgMain];
        [scrlDetail setContentSize:CGSizeMake(imgMain.rightEdge, scrlDetail.height)];
    }
    
}

-(void) countCost{
    CountCost* countCost = [CountCost new];
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:countCost];
    countCost.dicDetail = carDetail;
    [self presentViewController:navController animated:YES completion:nil];
}

-(void) backView{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return carDetail.count - 1;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    
    cell.textLabel.textColor = [UIColor hexColor:@"#686868"];
    cell.detailTextLabel.minimumScaleFactor = 0.5;
    
    if(indexPath.row == 0){
        cell.textLabel.text = @"Thương hiệu:";
        cell.detailTextLabel.text = [carDetail objectForKey:@"brand"];
    }
    if(indexPath.row == 1){
        cell.textLabel.text = @"Tên xe:";
        cell.detailTextLabel.text = [carDetail objectForKey:@"name"];
        cell.detailTextLabel.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    if(indexPath.row == 2){
        cell.textLabel.text = @"Loại xe";
        cell.detailTextLabel.text = [carDetail objectForKey:@"type"];
        cell.detailTextLabel.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    if(indexPath.row == 3){
        cell.textLabel.text = @"Dài x rộng x cao:";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (mm)",[carDetail objectForKey:@"size"]];
    }
    if(indexPath.row == 4){
        cell.textLabel.text = @"Hộp số";
        cell.detailTextLabel.text = [carDetail objectForKey:@"gear"];
    }
    if(indexPath.row == 5){
        cell.textLabel.text = @"Nguồn gốc";
        cell.detailTextLabel.text = [carDetail objectForKey:@"origin"];
        cell.detailTextLabel.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    if(indexPath.row == 6){
        cell.textLabel.text = @"Động cơ";
        cell.detailTextLabel.text = [carDetail objectForKey:@"engine"];
    }
    if(indexPath.row == 7){
        cell.textLabel.text = @"Công suất";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ mã lực",[carDetail objectForKey:@"power"]];
    }
    if(indexPath.row == 8){
        cell.textLabel.text = @"Momen";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (Nm)",[carDetail objectForKey:@"momen"]];
    }
    if(indexPath.row == 9){
        cell.textLabel.text = @"Khoảng sáng gầm";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (mm)",[carDetail objectForKey:@"ground"]];
    }
    if(indexPath.row == 10){
        cell.textLabel.text = @"Bình xăng";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (lít)",[carDetail objectForKey:@"gasoline"]];
    }
    if(indexPath.row == 11){
        cell.textLabel.text = @"Giá niêm yết";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ VND",[carDetail objectForKey:@"price"]];
        cell.detailTextLabel.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    if(indexPath.row == 12){
        cell.textLabel.text = @"Giá thương lượng";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ VND",[carDetail objectForKey:@"price_negotiate"]];
        cell.detailTextLabel.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    
    return cell;
}

-(IBAction) likeCar{
    btnLike.selected = !btnLike.selected;
    
    NSMutableArray* arrFav = [[self getFileWithKey:@"FAV_CAR"] mutableCopy];
    if(!arrFav && btnLike.selected){
        NSMutableArray* arrLike = [NSMutableArray new];
        [arrLike addObject:self.carDetail];
        [self writeFile:arrLike withKey:@"FAV_CAR"];
        return;
    }
    
    if(btnLike.selected){
        [arrFav addObject:self.carDetail];
    }else{
        [arrFav removeObject:self.carDetail];
    }
    
    [self writeFile:arrFav withKey:@"FAV_CAR"];
}

@end
