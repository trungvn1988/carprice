//
//  CountCost.m
//  CarPrice
//
//  Created by Trung 1988 on 2/8/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "CountCost.h"

@interface CountCost ()

@end

@implementation CountCost

@synthesize dicDetail, lblCost, txtArea;
@synthesize tblCount, taxNo, roadTax, priceDeal, insuranceTax, lisenceTax, lisenceFee;
@synthesize provincePicker;
@synthesize provincePickerBottom;
@synthesize arrProvince;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Chi phí dự tính";
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    tblCount.tableFooterView = [UIView new];
    txtArea.delegate = self;
    roadTax = 1500000;
    insuranceTax = 437000;
    taxNo = 12;
    priceDeal = [[[dicDetail objectForKey:@"price_negotiate"] stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue];
    lisenceTax = 20000000;
    lisenceFee = 340000;
    
    provincePickerBottom.constant = -provincePicker.height;
    
    UITapGestureRecognizer* tapHide = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePickerView)];
    [self.view addGestureRecognizer:tapHide];
    
    if(!arrProvince){
        arrProvince = [NSMutableArray new];
        [arrProvince removeAllObjects];
    }
    
    [arrProvince addObject:@"Hà Nội"];
    [arrProvince addObject:@"TP Hồ Chí Minh"];
    [arrProvince addObject:@"Hải Phòng"];
    [arrProvince addObject:@"Đà Nẵng"];
    [arrProvince addObject:@"Cần Thơ"];
    [arrProvince addObject:@"Bà Rịa"];
    [arrProvince addObject:@"Bạc Liêu"];
    [arrProvince addObject:@"Bảo Lộc"];
    [arrProvince addObject:@"Bắc Giang"];
    [arrProvince addObject:@"Bắc Cạn"];
    [arrProvince addObject:@"Bắc Ninh"];
    [arrProvince addObject:@"Bến Tre"];
    [arrProvince addObject:@"Biên Hòa"];
    [arrProvince addObject:@"Buôn Ma Thuột"];
    [arrProvince addObject:@"Cà Mau"];
    [arrProvince addObject:@"Cam Ranh"];
    [arrProvince addObject:@"Cao Bằng"];
    [arrProvince addObject:@"Cao Lãnh"];
    [arrProvince addObject:@"Cẩm Phả"];
    [arrProvince addObject:@"Châu Đốc"];
    [arrProvince addObject:@"Đà Lạt"];
    [arrProvince addObject:@"Điện Biên Phủ"];
    [arrProvince addObject:@"Đông Hà"];
    [arrProvince addObject:@"Đồng Hới"];
    [arrProvince addObject:@"Hà Giang"];
    [arrProvince addObject:@"Hạ Long"];
    [arrProvince addObject:@"Hà Tĩnh"];
    [arrProvince addObject:@"Hải Dương"];
    [arrProvince addObject:@"Hòa Bình"];
    [arrProvince addObject:@"Hội An"];
    [arrProvince addObject:@"Huế"];
    [arrProvince addObject:@"Hưng Yên"];
    [arrProvince addObject:@"Kon Tum"];
    [arrProvince addObject:@"Lai Châu"];
    [arrProvince addObject:@"Lạng Sơn"];
    [arrProvince addObject:@"Lào Cai"];
    [arrProvince addObject:@"Long Xuyên"];
    [arrProvince addObject:@"Móng Cái "];
    [arrProvince addObject:@"Mỹ Tho"];
    [arrProvince addObject:@"Nam Định"];
    [arrProvince addObject:@"Nha Trang"];
    [arrProvince addObject:@"Ninh Bình"];
    [arrProvince addObject:@"Phan Rang - Tháp Chàm"];
    [arrProvince addObject:@"Phan Thiết "];
    [arrProvince addObject:@"Phủ Lý"];
    [arrProvince addObject:@"Pleiku"];
    [arrProvince addObject:@"Quy Nhơn"];
    [arrProvince addObject:@"Rạch Giá"];
    [arrProvince addObject:@"Sa Đéc"];
    [arrProvince addObject:@"Sóc Trăng"];
    [arrProvince addObject:@"Sơn La"];
    [arrProvince addObject:@"Sông Công"];
    [arrProvince addObject:@"Tam Điệp"];
    [arrProvince addObject:@"Tam Kỳ"];
    [arrProvince addObject:@"Tân An"];
    [arrProvince addObject:@"Tây Ninh"];
    [arrProvince addObject:@"Thái Bình"];
    [arrProvince addObject:@"Thái Nguyên"];
    [arrProvince addObject:@"Thanh Hóa"];
    [arrProvince addObject:@"Thủ Dầu Một"];
    [arrProvince addObject:@"Trà Vinh"];
    [arrProvince addObject:@"Tuy Hòa"];
    [arrProvince addObject:@"Tuyên Quang"];
    [arrProvince addObject:@"Uông Bí"];
    [arrProvince addObject:@"Vị Thanh "];
    [arrProvince addObject:@"Việt Trì"];
    [arrProvince addObject:@"Vinh"];
    [arrProvince addObject:@"Vĩnh Long"];
    [arrProvince addObject:@"Vĩnh Yên"];
    [arrProvince addObject:@"Vũng Tàu"];
    
    [self selectRow:0];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];

    
    cell.textLabel.textColor = [UIColor hexColor:@"#686868"];

    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    if(indexPath.row == 0){
        cell.textLabel.text = @"Giá đàm phán:";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInteger:priceDeal]]];
    }
    if(indexPath.row == 1){
        cell.textLabel.text = [NSString stringWithFormat:@"Phí trước bạ (%li%%):", (long)taxNo];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInteger:priceDeal*taxNo/100]]];
    }
    
    if(indexPath.row == 2){
        cell.textLabel.text = [NSString stringWithFormat:@"Phí đường bộ (1 năm):"];

        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInteger:roadTax]]];
    }
    if(indexPath.row == 3){
        cell.textLabel.text = [NSString stringWithFormat:@"Bảo hiểm dân sự (1 năm)"];

        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInteger:insuranceTax]]];
    }
    if(indexPath.row == 4){
        cell.textLabel.text = [NSString stringWithFormat:@"Đăng ký biển số"];

        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInteger:lisenceTax]]];
    }
    if(indexPath.row == 5){
        cell.textLabel.text = [NSString stringWithFormat:@"Phí đăng kiểm"];

        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInteger:lisenceFee]]];
    }
    if(indexPath.row == 6){
        cell.textLabel.text = [NSString stringWithFormat:@"Tổng chi phí"];

        NSInteger total = lisenceFee + lisenceTax + insuranceTax + roadTax + priceDeal*taxNo/100 + priceDeal;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInteger:total]]];
        
        cell.textLabel.textColor = [UIColor hexColor:@"#CD5C5C"];

        cell.detailTextLabel.textColor = [UIColor hexColor:@"#CD5C5C"];

        cell.textLabel.font = [UIFont boldSystemFontOfSize:cell.textLabel.font.pointSize];
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:cell.detailTextLabel.font.pointSize];
    }
    
    return cell;
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    provincePickerBottom.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    return NO;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arrProvince.count;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [arrProvince objectAtIndex:row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self selectRow:row];
}

-(void) selectRow:(NSInteger) row{
    switch (row) {
        case 0:
            taxNo = 12;
            lisenceTax = 20000000;
            break;
        case 1:
            taxNo = 10;
            lisenceTax = 11000000;
            break;
            
        default:
            taxNo = 10;
            lisenceTax = 1000000;
            break;
    }
    
    txtArea.text = [arrProvince objectAtIndex:row];
    [tblCount reloadData];
}

-(void) hidePickerView{
    provincePickerBottom.constant = -provincePicker.height;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
