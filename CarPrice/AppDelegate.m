//
//  AppDelegate.m
//  CarPrice
//
//  Created by Trung 1988 on 2/2/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window;

@synthesize fullScreenAd, pendingSelector, targetView;

@synthesize interstitial;

@synthesize localNotif;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [FIRApp configure];
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-2022191528301116~2246679380"];
    
    UITabBarController* tabController = (UITabBarController*)window.rootViewController;
    
    UITabBarItem* item0 = [tabController.tabBar.items objectAtIndex:0];
    item0.image = [[UIImage imageNamed:@"tabowner"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item0.selectedImage = [[UIImage imageNamed:@"tabowner"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item0.title = @"Xe của tôi";
    
    UITabBarItem* item1 = [tabController.tabBar.items objectAtIndex:1];
    item1.image = [[UIImage imageNamed:@"tabshowroom"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item1.selectedImage = [[UIImage imageNamed:@"tabshowroom"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item1.title = @"Showroom";
    
    UITabBarItem* item2 = [tabController.tabBar.items objectAtIndex:2];
    item2.image = [[UIImage imageNamed:@"tabhome"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item2.selectedImage = [[UIImage imageNamed:@"tabhome"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item2.title = @"Thương Hiệu";
    
    UITabBarItem* item3 = [tabController.tabBar.items objectAtIndex:3];
    item3.image = [[UIImage imageNamed:@"tabcompare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item3.selectedImage = [[UIImage imageNamed:@"tabcompare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item3.title = @"So Sánh";
    
    UITabBarItem* item4 = [tabController.tabBar.items objectAtIndex:4];
    item4.image = [[UIImage imageNamed:@"tabnews"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item4.selectedImage = [[UIImage imageNamed:@"tabnews"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item4.title = @"Bản tin";
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"sorttype"]){
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"sorttype"];
    }
    
    tabController.selectedIndex = 2;
    [Fabric with:@[[Crashlytics class]]];
    [self registerPush];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self setLocalNotification];
    
    // Override point for customization after application launch.
    
    [Appirater setAppId:@"1206188964"];
    [Appirater setDaysUntilPrompt:1];
    [Appirater setUsesUntilPrompt:3];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:2];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];
    
    return YES;
}

-(void) registerPush{
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        // For iOS 10 data message (sent via FCM)
        [FIRMessaging messaging].remoteMessageDelegate = self;
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    NSLog(@"refreshedToken %@", refreshedToken);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"deviceToken %@", deviceToken);
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
//    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}

-(void) applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage{
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    [Appirater appEnteredForeground:YES];
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [self setLocalNotification];
    application.applicationIconBadgeNumber = 0;
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)loadInterstitial{
    [window addLoadingView];
    self.interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-2022191528301116/5978874988"];
    self.interstitial.delegate = self;
    
    GADRequest *request = [GADRequest request];
    [self.interstitial loadRequest:request];
}

//admob
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad{
    [window removeLoadingView];
    if(self.interstitial.isReady){
        [self.interstitial presentFromRootViewController:self.window.rootViewController];
    }
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error{
    [window removeLoadingView];
    fullScreenAd = [[FBInterstitialAd alloc] initWithPlacementID:@"1765263163791610_1765266320457961"];
    fullScreenAd.delegate = self;
    [fullScreenAd loadAd];
}

-(void) interstitialWillDismissScreen:(GADInterstitial *)ad{
    if(targetView && pendingSelector){
        [targetView performSelectorOnMainThread:pendingSelector withObject:nil waitUntilDone:NO];
        pendingSelector = nil;
    }
}

//Facebook
-(void) interstitialAdDidLoad:(FBInterstitialAd *)interstitialAd{
    [interstitialAd showAdFromRootViewController:window.rootViewController];
    [window removeLoadingView];
}

- (void)interstitialAd:(FBInterstitialAd *)interstitialAd
      didFailWithError:(NSError *)error{
    NSLog(@"Interstitial ad is failed to load with error: %@", error);
    [window removeLoadingView];
    if(targetView && pendingSelector){
        [targetView performSelectorOnMainThread:pendingSelector withObject:nil waitUntilDone:NO];
        pendingSelector = nil;
    }
}

-(void) interstitialAdDidClick:(FBInterstitialAd *)interstitialAd{
    NSLog(@"Ad click");
}

-(void) interstitialAdWillLogImpression:(FBInterstitialAd *)interstitialAd{
    NSLog(@"Log Impression");
}

-(void) interstitialAdDidClose:(FBInterstitialAd *)interstitialAd{
    if(targetView && pendingSelector){
        [targetView performSelectorOnMainThread:pendingSelector withObject:nil waitUntilDone:NO];
        pendingSelector = nil;
    }
}

-(BOOL) showAd:(nullable SEL) pendingTask
        target:(nullable id) target{
    
    pendingSelector = pendingTask;
    targetView = target;
    
    NSDate* adDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"adDate"];
    if(!adDate){
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                 message:@"Nhằm ủng hộ cho đội ngũ lập trình viên.Mong các bạn xem quảng cáo vài giây sau đây\n*Quảng cáo toàn màn hình cách nhau tối thiểu 15 phút*.\nXin trân trọng cảm ơn!"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Đồng ý" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self loadInterstitial];
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"adDate"];
        }]];
        
        [window.rootViewController presentViewController:alertController animated:YES completion:nil];
        
        return YES;
    }
    
    NSTimeInterval gap = [[NSDate date] timeIntervalSinceDate:adDate];
    NSLog(@"gap time %f", gap);
    if(gap > 900){
        [self loadInterstitial];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"adDate"];
        return YES;
    }
    return NO;
}

- (void)setLocalNotification {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSInteger timeRandom = arc4random()%3;
    
    NSDate* currentDate = [[NSCalendar currentCalendar] startOfDayForDate:[NSDate date]];
    NSDate* fireDate = [NSDate dateWithTimeInterval:(timeRandom+12)*3600 sinceDate:currentDate];
    
    if([[NSDate date] timeIntervalSince1970] > [fireDate timeIntervalSince1970]){
        //over time, set fire date for tomorrow.
        NSDate* tomorowDate = [[NSCalendar currentCalendar] startOfDayForDate:[[NSDate date] dateByAddingTimeInterval:86400]];
        fireDate = [NSDate dateWithTimeInterval:(timeRandom+12)*3600 sinceDate:tomorowDate];
    }
    
    localNotif = [UILocalNotification new];
    localNotif.fireDate = fireDate;
    localNotif.timeZone = [NSTimeZone systemTimeZone];
    
    NSString* content = @"";
    NSInteger random = arc4random()%3;
    switch (random) {
        case 0:
            content = @"Bản tin xe mới nhất, hot nhất.";
            break;
        case 1:
            content = @"Đọc báo về xe mỗi ngày. Cập nhật ngay hôm nay.";
            break;
        case 2:
            content = @"Bản tin trưa về xế hộp trong tầm tay.";
            break;
            
        default:
            break;
    }
    
    //Payload
    localNotif.alertBody = content;
    localNotif.hasAction = YES;
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.repeatInterval = NSCalendarUnitDay;
    
    NSInteger badgeNo = [UIApplication sharedApplication].applicationIconBadgeNumber;
    localNotif.applicationIconBadgeNumber = badgeNo+1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}


@end
