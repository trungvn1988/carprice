//
//  MaintainVC.m
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "MaintainVC.h"

@interface MaintainVC ()

@end

@implementation MaintainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Lịch sử bảo dưỡng";
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem createRightButtonImage:[UIImage imageNamed:@"add-icon"]
                                                                              Target:self
                                                                              Action:@selector(addMaintainHistory)];
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) addMaintainHistory{
    
}

@end
