//
//  GasVC.m
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "GasVC.h"

@interface GasVC ()

@end

@implementation GasVC

@synthesize tblGasGistory;
@synthesize lblNoInfo;
@synthesize arrGasHistory;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Lịch sử đổ xăng";
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem createRightButtonImage:[UIImage imageNamed:@"add-icon"]
                                                                              Target:self
                                                                              Action:@selector(addGasHistory)];
    
    [self loadContent];
    
    tblGasGistory.tableFooterView = [UIView new];
    
    tblGasGistory.estimatedRowHeight = 50;
    tblGasGistory.rowHeight = UITableViewAutomaticDimension;
    
    [tblGasGistory registerNib:[UINib nibWithNibName:@"GasVCCell" bundle:nil] forCellReuseIdentifier:@"GasVCCell"];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadContent];
}

-(void) loadContent{
    arrGasHistory = [[self getFileWithKey:@"GAS"] mutableCopy];
    
    NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"gas_date" ascending:NO];
    arrGasHistory = [[arrGasHistory sortedArrayUsingDescriptors:@[descriptor]] mutableCopy];
    
    lblNoInfo.hidden = (arrGasHistory.count > 0);
    [tblGasGistory reloadData];
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) addGasHistory{
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:[AddGasVC new]];
    self.navigationItem.rightBarButtonItem.enabled = FALSE;
    [self presentViewController:navController animated:YES completion:^{
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    }];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrGasHistory.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"GasVCCell";
    GasVCCell *cell = [tblGasGistory dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (!cell) {
        cell = [GasVCCell new];
    }
    
    cell.dicValue = [arrGasHistory objectAtIndex: indexPath.row];
    [cell drawCell];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [arrGasHistory removeObjectAtIndex:indexPath.row];
        [self writeFile:arrGasHistory withKey:@"GAS"];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self loadContent];
        }];
    }
}

@end
