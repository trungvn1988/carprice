//
//  AddGasVC.m
//  CarPrice
//
//  Created by Trung 1988 on 5/5/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "AddGasVC.h"

@interface AddGasVC ()

@end

@implementation AddGasVC

@synthesize txtDate, txtLitre, txtMoney, txtGasType;
@synthesize datePicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    self.title = @"Thêm lịch sử";
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [txtDate setInputView:datePicker];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeEdit)];
    [self.view addGestureRecognizer:tapGesture];
}

-(void) closeEdit{
    [self.view endEditing:YES];
}

-(void)updateTextField:(id)sender{
    UIDatePicker *picker = (UIDatePicker*)txtDate.inputView;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/YYYY"];
    txtDate.text = [formatter stringFromDate:picker.date];
}

-(void) textFieldDidBeginEditing:(UITextField *)textField{
    if([textField isEqual:txtDate]){
        UIDatePicker *picker = (UIDatePicker*)txtDate.inputView;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/YYYY"];
        txtDate.text = [formatter stringFromDate:picker.date];
    }
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction) addHistory{
    if(txtDate.text.length == 0){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Xin chọn ngày"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Đồng ý" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    if(txtLitre.text.length == 0){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Xin điền số lít đã đổ"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Đồng ý" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    if(txtMoney.text.length == 0){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Xin điền số tiền sử dụng"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Đồng ý" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    NSMutableArray* arrGasHistory = [[self getFileWithKey:@"GAS"] mutableCopy];
    if(!arrGasHistory){
        arrGasHistory = [NSMutableArray new];
    }
    
    NSMutableDictionary* dicGasHistory = [NSMutableDictionary new];
    [dicGasHistory setValue:((UIDatePicker*)txtDate.inputView).date forKey:@"gas_date"];
    [dicGasHistory setValue:txtLitre.text forKey:@"gas_litre"];
    [dicGasHistory setValue:txtMoney.text forKey:@"gas_money"];
    [dicGasHistory setValue:txtGasType.text forKey:@"gas_type"];
    
    [arrGasHistory addObject:dicGasHistory];
    
    [self writeFile:arrGasHistory withKey:@"GAS"];
    
    [self backView];
}

@end
