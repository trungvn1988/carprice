//
//  GasVCCell.m
//  CarPrice
//
//  Created by Trung 1988 on 5/5/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "GasVCCell.h"

@implementation GasVCCell

@synthesize lblDate, lblLitre, lblMoney, lblGasType;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@synthesize dicValue;

-(void) drawCell{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *date = [formatter stringFromDate:[dicValue objectForKey:@"gas_date"]];
    
    lblDate.text = [NSString stringWithFormat:@"Ngày đổ: %@",date];
    lblGasType.text = [NSString stringWithFormat:@"Loại xăng: %@", [dicValue objectForKey:@"gas_type"]];
    lblMoney.text = [NSString stringWithFormat:@"%@ VND", [dicValue objectForKey:@"gas_money"]];
    lblLitre.text = [NSString stringWithFormat:@"Số lượng: %@ (lít)", [dicValue objectForKey:@"gas_litre"]];
}

@end
