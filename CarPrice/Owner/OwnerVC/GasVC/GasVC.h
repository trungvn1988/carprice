//
//  GasVC.h
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddGasVC.h"
#import "GasVCCell.h"

@interface GasVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonnull, retain) IBOutlet UITableView* tblGasGistory;
@property (nonnull, retain) IBOutlet UILabel* lblNoInfo;

@property (nonnull, retain) NSMutableArray* arrGasHistory;

@end
