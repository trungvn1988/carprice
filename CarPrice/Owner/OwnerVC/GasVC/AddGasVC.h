//
//  AddGasVC.h
//  CarPrice
//
//  Created by Trung 1988 on 5/5/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddGasVC : UIViewController <UITextFieldDelegate>

@property (nonnull, retain) IBOutlet UITextField* txtDate;
@property (nonnull, retain) IBOutlet UITextField* txtLitre;
@property (nonnull, retain) IBOutlet UITextField* txtMoney;
@property (nonnull, retain) IBOutlet UITextField* txtGasType;

@property (nonnull, retain) UIButton* btnAddHistory;

@property (nonnull, retain) UIDatePicker* datePicker;

-(IBAction) addHistory;

@end
