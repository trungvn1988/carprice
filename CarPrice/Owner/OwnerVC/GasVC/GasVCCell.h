//
//  GasVCCell.h
//  CarPrice
//
//  Created by Trung 1988 on 5/5/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GasVCCell : UITableViewCell

@property (nonnull, retain) NSDictionary* dicValue;

@property (nonnull, retain) IBOutlet UILabel* lblDate;
@property (nonnull, retain) IBOutlet UILabel* lblLitre;
@property (nonnull, retain) IBOutlet UILabel* lblMoney;
@property (nonnull, retain) IBOutlet UILabel* lblGasType;

-(void) drawCell;

@end
