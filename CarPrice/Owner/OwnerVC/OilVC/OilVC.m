//
//  OilVC.m
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "OilVC.h"

@interface OilVC ()

@end

@implementation OilVC

@synthesize lblNoInfo;
@synthesize arrOilHistory, tblOilHistory;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Lịch sử thay dầu";
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem createRightButtonImage:[UIImage imageNamed:@"add-icon"]
                                                                            Target:self
                                                                            Action:@selector(addOilHistory)];
    
    tblOilHistory.tableFooterView = [UIView new];
    tblOilHistory.estimatedRowHeight = 50;
    tblOilHistory.rowHeight = UITableViewAutomaticDimension;
    
    [tblOilHistory registerNib:[UINib nibWithNibName:@"OilVCCell" bundle:nil] forCellReuseIdentifier:@"OilVCCell"];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadContent];
}

-(void) reloadContent{
    arrOilHistory = [[self getFileWithKey:@"OIL"] mutableCopy];
    
    NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"oil_date" ascending:NO];
    arrOilHistory = [[arrOilHistory sortedArrayUsingDescriptors:@[descriptor]] mutableCopy];
    
    lblNoInfo.hidden = (arrOilHistory.count > 0);
    [tblOilHistory reloadData];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrOilHistory.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"OilVCCell";
    OilVCCell *cell = [tblOilHistory dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (!cell) {
        cell = [OilVCCell new];
    }
    
    cell.dicValue = [arrOilHistory objectAtIndex: indexPath.row];
    [cell drawCell];
    
    return cell;
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) addOilHistory{
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:[AddOilVC new]];
    self.navigationItem.rightBarButtonItem.enabled = FALSE;
    [self presentViewController:navController animated:YES completion:^{
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    }];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [arrOilHistory removeObjectAtIndex:indexPath.row];
        [self writeFile:arrOilHistory withKey:@"OIL"];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self reloadContent];
        }];
    }
}

@end
