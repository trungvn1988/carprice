//
//  AddOilVC.m
//  CarPrice
//
//  Created by Trung 1988 on 5/5/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "AddOilVC.h"

@interface AddOilVC ()

@end

@implementation AddOilVC

@synthesize txtDate, txtOiltype, txtOilPrice;

@synthesize btnAddOil, datePicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    self.title = @"Thêm lịch sử";
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [txtDate setInputView:datePicker];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeEdit)];
    [self.view addGestureRecognizer:tapGesture];
}

-(void) closeEdit{
    [self.view endEditing:YES];
}

-(void)updateTextField:(id)sender{
    UIDatePicker *picker = (UIDatePicker*)txtDate.inputView;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/YYYY"];
    txtDate.text = [formatter stringFromDate:picker.date];
}

-(void) textFieldDidBeginEditing:(UITextField *)textField{
    if([textField isEqual:txtDate]){
        UIDatePicker *picker = (UIDatePicker*)txtDate.inputView;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/YYYY"];
        txtDate.text = [formatter stringFromDate:picker.date];
    }
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction) addHistory{
    if(txtDate.text.length == 0){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Xin chọn ngày"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Đồng ý" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    if(txtOiltype.text.length == 0){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Xin điền tên loại dầu bạn đã thay"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Đồng ý" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    if(txtOilPrice.text.length == 0){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Xin điền số tiền sử dụng"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Đồng ý" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    NSMutableArray* arrOilHistory = [[self getFileWithKey:@"OIL"] mutableCopy];
    if(!arrOilHistory){
        arrOilHistory = [NSMutableArray new];
    }
    
    NSMutableDictionary* dicOilHistory = [NSMutableDictionary new];
    [dicOilHistory setValue:((UIDatePicker*)txtDate.inputView).date forKey:@"oil_date"];
    [dicOilHistory setValue:txtOilPrice.text forKey:@"oil_money"];
    [dicOilHistory setValue:txtOiltype.text forKey:@"oil_type"];
    
    [arrOilHistory addObject:dicOilHistory];
    
    [self writeFile:arrOilHistory withKey:@"OIL"];
    
    [self backView];
}


@end
