//
//  OilVC.h
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddOilVC.h"
#import "OilVCCell.h"

@interface OilVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonnull, retain) IBOutlet UILabel* lblNoInfo;

@property (nonnull, retain) NSMutableArray* arrOilHistory;

@property (nonnull, retain) IBOutlet UITableView* tblOilHistory;

-(void) reloadContent;

@end
