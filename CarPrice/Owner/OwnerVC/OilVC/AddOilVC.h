//
//  AddOilVC.h
//  CarPrice
//
//  Created by Trung 1988 on 5/5/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddOilVC : UIViewController <UITextFieldDelegate>

@property (nonnull, retain) IBOutlet UITextField* txtDate;
@property (nonnull, retain) IBOutlet UITextField* txtOiltype;
@property (nonnull, retain) IBOutlet UITextField* txtOilPrice;

@property (nonnull, retain) IBOutlet UIButton* btnAddOil;

@property (nonnull, retain) UIDatePicker* datePicker;

-(IBAction) addHistory;

@end
