//
//  OilVCCell.m
//  CarPrice
//
//  Created by Trung 1988 on 5/5/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "OilVCCell.h"

@implementation OilVCCell

@synthesize dicValue;
@synthesize lblDate, lblOilType, lblOilPrice;

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void) drawCell{
//    NSLog(@"dicValue %@", dicValue);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *date = [formatter stringFromDate:[dicValue objectForKey:@"oil_date"]];
    
    lblDate.text = [NSString stringWithFormat:@"Ngày đổ: %@",date];
    lblOilType.text = [NSString stringWithFormat:@"Loại dầu: %@", [dicValue objectForKey:@"oil_type"]];
    lblOilPrice.text = [NSString stringWithFormat:@"Tiền: %@ VND", [dicValue objectForKey:@"oil_money"]];
}

@end
