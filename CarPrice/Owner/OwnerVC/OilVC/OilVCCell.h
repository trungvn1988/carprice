//
//  OilVCCell.h
//  CarPrice
//
//  Created by Trung 1988 on 5/5/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OilVCCell : UITableViewCell

@property (nonnull, retain) NSDictionary* dicValue;

@property (nonnull, retain) IBOutlet UILabel* lblDate;
@property (nonnull, retain) IBOutlet UILabel* lblOilType;
@property (nonnull, retain) IBOutlet UILabel* lblOilPrice;

-(void) drawCell;

@end
