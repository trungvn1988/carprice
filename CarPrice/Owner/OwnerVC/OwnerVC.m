//
//  OwnerVC.m
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "OwnerVC.h"

@interface OwnerVC ()

@end

@implementation OwnerVC

@synthesize myCarView;

@synthesize lblMyName, lblMyCarYear, lblMyCarBrand, lblMyCarPlate;
@synthesize imgMyCar;

@synthesize imgPicker, txtContent;
@synthesize currentLabelType;

@synthesize tblCarMaintain;

@synthesize loadedAd, bannerView, adView, adViewAll, adViewHeight;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Xe của tôi.";
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem createRightButtonImage:[UIImage imageNamed:@"heart-icon"]
                                                                              Target:self
                                                                              Action:@selector(selectFavouriteList)];
    
    imgMyCar.layer.cornerRadius = 5;
    [imgMyCar setBorder:1 color:[UIColor whiteColor]];
    
    imgPicker = [UIImagePickerController new];
    imgPicker.navigationBar.tintColor = [UIColor whiteColor];
    imgPicker.delegate = self;
    
    txtContent.delegate = self;
    [txtContent addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    UITapGestureRecognizer* tapImageCar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAddImage)];
    [imgMyCar addGestureRecognizer:tapImageCar];
    
    UITapGestureRecognizer* tapLabel1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLabel:)];
    [lblMyName addGestureRecognizer:tapLabel1];
    
    UITapGestureRecognizer* tapLabel2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLabel:)];
    [lblMyCarBrand addGestureRecognizer:tapLabel2];
    
    UITapGestureRecognizer* tapLabel3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLabel:)];
    [lblMyCarYear addGestureRecognizer:tapLabel3];
    
    UITapGestureRecognizer* tapLabel4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLabel:)];
    [lblMyCarPlate addGestureRecognizer:tapLabel4];
    
    [self checkProfile];
    
    tblCarMaintain.tableFooterView = [UIView new];
    [tblCarMaintain registerNib:[UINib nibWithNibName:@"OwnerCell" bundle:nil] forCellReuseIdentifier:@"OwnerCell"];
    
    adViewHeight.constant = 0.0;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!loadedAd)
        [self loadAd];
}

-(void) loadAd{
    self.bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 50)];
    self.bannerView.adUnitID = @"ca-app-pub-2022191528301116/3723412583";
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    GADRequest* request = [GADRequest request];
    [self.bannerView loadRequest:request];
}

//Google Ads
-(void) adViewDidReceiveAd:(GADBannerView *)bannerView2{
    loadedAd = TRUE;
    [adViewAll addSubview:bannerView];
    adViewHeight.constant = 50.0;
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    adView = [[FBAdView alloc] initWithPlacementID:@"1765263163791610_1777642235887036"
                                            adSize:kFBAdSizeHeight50Banner
                                rootViewController:self];
    adView.delegate = self;
    [adView loadAd];
}

//Facebook Ads
- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error{
    NSLog(@"error %@", error);
}

- (void)adViewDidLoad:(FBAdView *)adView2{
    loadedAd = TRUE;
    [adViewAll addSubview:adView];
    adViewHeight.constant = 50.0;
}

-(void) adViewDidClick:(FBAdView *)adView{
    NSLog(@"Banner ad was clicked.");
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    NSLog(@"Banner ad did finish click handling.");
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    NSLog(@"Banner ad impression is being captured.");
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"OwnerCell";
    OwnerCell *cell = [tblCarMaintain dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (!cell) {
        cell = [OwnerCell new];
    }
    
    switch (indexPath.row) {
        case 0:{
            cell.imgIcon.image = [UIImage imageNamed:@"gas-icon"];
            cell.lblTitle.text = @"Lịch sử đổ xăng";
        }
            break;
        case 1:{
            cell.imgIcon.image = [UIImage imageNamed:@"oilchange-icon"];
            cell.lblTitle.text = @"Lịch sử thay dầu";
        }
            break;
        case 2:{
            cell.imgIcon.image = [UIImage imageNamed:@"maintain-icon"];
            cell.lblTitle.text = @"Bảo dưỡng định kỳ";
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            GasVC* gasVC = [GasVC new];
            [self.navigationController pushViewController:gasVC animated:YES];
        }
            break;
        case 1:{
            OilVC* oilVC = [OilVC new];
            [self.navigationController pushViewController:oilVC animated:YES];
        }
            break;
        case 2:{
            MaintainVC* maintainVC = [MaintainVC new];
            [self.navigationController pushViewController:maintainVC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

-(void) checkProfile{
    NSMutableDictionary* dicProfile = [self getFileWithKey:@"PROFILE"];
    if(!dicProfile){
        dicProfile = [NSMutableDictionary new];
        [dicProfile setObject:@"" forKey:@"name"];
        [dicProfile setObject:@"" forKey:@"car_brand"];
        [dicProfile setObject:@"2017" forKey:@"car_year"];
        [dicProfile setObject:@"" forKey:@"car_plate"];
        [self writeFile:dicProfile withKey:@"PROFILE"];
    }
    
    [self drawContent];
}

-(void) tapLabel:(UIGestureRecognizer*) tapGesture{
    NSMutableDictionary* dicProfile = [self getFileWithKey:@"PROFILE"];
    [txtContent becomeFirstResponder];
    if([tapGesture.view isEqual:lblMyName]){
        currentLabelType = kName;
        txtContent.text = [dicProfile objectForKey:@"name"];
    }
    
    if([tapGesture.view isEqual:lblMyCarBrand]){
        currentLabelType = kCarBrand;
        txtContent.text = [dicProfile objectForKey:@"car_brand"];
    }
    
    if([tapGesture.view isEqual:lblMyCarYear]){
        currentLabelType = kCarYear;
        txtContent.text = [dicProfile objectForKey:@"car_year"];
    }
    
    if([tapGesture.view isEqual:lblMyCarPlate]){
        currentLabelType = kCarPlate;
        txtContent.text = [dicProfile objectForKey:@"car_plate"];
    }
}

-(void) textFieldDidChange:(id) sender{
    switch (currentLabelType) {
        case kName:{
            lblMyName.text = [NSString stringWithFormat:@"Chủ xe: %@", txtContent.text];
        }
            break;
        case kCarBrand:{
            lblMyCarBrand.text = [NSString stringWithFormat:@"Loại xe: %@", txtContent.text];
        }
            break;
        case kCarYear:{
            lblMyCarYear.text = [NSString stringWithFormat:@"Đời xe: %@", txtContent.text];
        }
            break;
        case kCarPlate:{
            lblMyCarPlate.text = [NSString stringWithFormat:@"Biển số xe: %@", txtContent.text];
        }
            break;
            
        default:
            break;
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    NSMutableDictionary* dicProfile = [[self getFileWithKey:@"PROFILE"] mutableCopy];
    [txtContent resignFirstResponder];
    switch (currentLabelType) {
        case kName:
            [dicProfile setObject:txtContent.text forKey:@"name"];
            break;
        case kCarBrand:
            [dicProfile setObject:txtContent.text forKey:@"car_brand"];
            break;
        case kCarYear:
            [dicProfile setObject:txtContent.text forKey:@"car_year"];
            break;
        case kCarPlate:
            [dicProfile setObject:txtContent.text forKey:@"car_plate"];
            break;
            
        default:
            break;
    }
    txtContent.text = @"";
    [self writeFile:dicProfile withKey:@"PROFILE"];
    
    return YES;
}

-(void) tapAddImage{
    UIAlertController* alertView = [UIAlertController alertControllerWithTitle:nil message:@"Cập nhật ảnh" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertView addAction:[UIAlertAction actionWithTitle:@"Thư viện ảnh" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }]];
    
    [alertView addAction:[UIAlertAction actionWithTitle:@"Máy ảnh" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }]];
    
    [alertView addAction:[UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    
    [self presentViewController:alertView animated:YES completion:nil];
}

-(void) drawContent{
    NSMutableDictionary* dicProfile = [self getFileWithKey:@"PROFILE"];
    
    lblMyName.text = [NSString stringWithFormat:@"Chủ xe: %@", [dicProfile objectForKey:@"name"]];
    lblMyCarBrand.text = [NSString stringWithFormat:@"Loại xe: %@", [dicProfile objectForKey:@"car_brand"]];
    lblMyCarYear.text = [NSString stringWithFormat:@"Đời xe: %@", [dicProfile objectForKey:@"car_year"]];
    lblMyCarPlate.text = [NSString stringWithFormat:@"Biển số xe: %@", [dicProfile objectForKey:@"car_plate"]];
    
    if([dicProfile objectForKey:@"car_image"])
        imgMyCar.image = [UIImage imageWithData:[dicProfile objectForKey:@"car_image"]];
}

-(void) selectFavouriteList{
    [self.navigationController pushViewController:[FavouriteVC new] animated:YES];
}

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [imgPicker dismissViewControllerAnimated:YES completion:nil];
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [imgPicker dismissViewControllerAnimated:YES completion:nil];
    UIImage* imgCar = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if(imgCar){
        NSMutableDictionary* dicProfile = [[self getFileWithKey:@"PROFILE"] mutableCopy];
        [dicProfile setObject:UIImageJPEGRepresentation(imgCar, 0.7) forKey:@"car_image"];
        [self writeFile:dicProfile withKey:@"PROFILE"];
    }
    [self drawContent];
}

@end
