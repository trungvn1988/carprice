//
//  OwnerVC.h
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteVC.h"
#import "OwnerCell.h"
#import "GasVC.h"
#import "OilVC.h"
#import "MaintainVC.h"

@interface OwnerVC : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, FBAdViewDelegate, GADBannerViewDelegate>

@property (nonnull, retain) IBOutlet UIView* myCarView;
@property (nonnull, retain) IBOutlet UIImageView* imgMyCar;
@property (nonnull, retain) IBOutlet UILabel* lblMyName;
@property (nonnull, retain) IBOutlet UILabel* lblMyCarBrand;
@property (nonnull, retain) IBOutlet UILabel* lblMyCarYear;
@property (nonnull, retain) IBOutlet UILabel* lblMyCarPlate;

@property (nonnull, retain) IBOutlet UITableView* tblCarMaintain;

-(void) tapAddImage;

@property (nullable, retain) UIImagePickerController* imgPicker;
@property (nonnull, retain) IBOutlet UITextField* txtContent;

@property (nonnull, retain) IBOutlet UIView* adViewAll;
@property (nonnull, retain) IBOutlet NSLayoutConstraint* adViewHeight;


@property (nonnull, retain) FBAdView *adView;
@property (nullable, retain) GADBannerView  *bannerView;
@property BOOL loadedAd;

@property NSInteger currentLabelType;

typedef enum {
    kName,
    kCarBrand,
    kCarYear,
    kCarPlate,
} lblType;

@end
