//
//  OwnerCell.h
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerCell : UITableViewCell

@property (nonnull, retain) IBOutlet UIImageView* imgIcon;
@property (nonnull, retain) IBOutlet UILabel* lblTitle;

@end
