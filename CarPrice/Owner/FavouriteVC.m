//
//  FavouriteVC.m
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "FavouriteVC.h"

@interface FavouriteVC ()

@end

@implementation FavouriteVC

@synthesize tblFavour;
@synthesize loadedAd, arrFavourite, adView, bannerView;
@synthesize lblAlert;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tblFavour.estimatedRowHeight = 50;
    tblFavour.rowHeight = UITableViewAutomaticDimension;
    
    [tblFavour registerNib:[UINib nibWithNibName:@"BrandListCell" bundle:nil] forCellReuseIdentifier:@"BrandListCell"];
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem createLeftButtonImage:[UIImage imageNamed:@"back"]
                                                                            Target:self
                                                                            Action:@selector(backView)];
    tblFavour.tableFooterView = [UIView new];
    loadedAd = FALSE;
    
    self.title = @"Danh sách ưa thích";
}

-(void) backView{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    arrFavourite = [self getFileWithKey:@"FAV_CAR"];
    lblAlert.hidden = (arrFavourite.count > 0);
    
    [tblFavour reloadData];
    [self loadAd];
}

-(void) loadAd{
    self.bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 50)];
    self.bannerView.adUnitID = @"ca-app-pub-2022191528301116/3723412583";
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    GADRequest* request = [GADRequest request];
    [self.bannerView loadRequest:request];
}

//Google Ads
-(void) adViewDidReceiveAd:(GADBannerView *)bannerView{
    loadedAd = TRUE;
    [tblFavour reloadData];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerView = nil;
    adView = [[FBAdView alloc] initWithPlacementID:@"1765263163791610_1777642235887036"
                                            adSize:kFBAdSizeHeight50Banner
                                rootViewController:self];
    adView.delegate = self;
    [adView loadAd];
}

- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error{
    NSLog(@"error %@", error);
}

- (void)adViewDidLoad:(FBAdView *)adView;{
    loadedAd = TRUE;
    [tblFavour reloadData];
}

-(void) adViewDidClick:(FBAdView *)adView{
    NSLog(@"Banner ad was clicked.");
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView{
    NSLog(@"Banner ad did finish click handling.");
}

- (void)adViewWillLogImpression:(FBAdView *)adView{
    NSLog(@"Banner ad impression is being captured.");
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(arrFavourite.count == 0)
        return 0;
    
    if(loadedAd)
        return arrFavourite.count + 1;
    return arrFavourite.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"BrandListCell";
    
    if(indexPath.row == 0){
        BrandListCell *cell = [tblFavour dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (!cell) {
            cell = [BrandListCell new];
        }
        
        cell.dicCar = [arrFavourite objectAtIndex:indexPath.row];
        [cell drawCell];
        return cell;
    }
    
    if(loadedAd && indexPath.row == 1){
        UITableViewCell* cell = [UITableViewCell new];
        if(bannerView){
            [cell.contentView addSubview:bannerView];
        }
        else{
            [cell.contentView addSubview:adView];
        }
        
        return cell;
    }
    
    NSInteger cellIndex = indexPath.row;
    if(loadedAd){
        cellIndex = cellIndex - 1;
    }
    
    BrandListCell *cell = [tblFavour dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (!cell) {
        cell = [BrandListCell new];
    }
    
    cell.dicCar = [arrFavourite objectAtIndex:cellIndex];
    [cell drawCell];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger cellIndex = indexPath.row;
    if(loadedAd){
        cellIndex = cellIndex - 1;
    }
    if(cellIndex < 0)
        cellIndex = 0;
    BrandListCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    if(![cell isKindOfClass:[BrandListCell class]])
        return;
    CarDetail* carDetail = [CarDetail new];
    carDetail.carDetail = cell.dicCar;
    [self.navigationController pushViewController:carDetail animated:YES];
}

@end
