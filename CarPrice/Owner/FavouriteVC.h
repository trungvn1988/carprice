//
//  FavouriteVC.h
//  CarPrice
//
//  Created by Trung 1988 on 5/4/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrandListCell.h"
#import "CarDetail.h"

@interface FavouriteVC : UIViewController <UITableViewDelegate, UITableViewDataSource, FBAdViewDelegate, GADBannerViewDelegate>

@property (nonnull, retain) IBOutlet UITableView* tblFavour;

@property (nonnull, retain) NSMutableArray* arrFavourite;

@property (nonnull, retain) FBAdView *adView;
@property (nullable, retain) IBOutlet GADBannerView  *bannerView;
@property (nonnull, retain) IBOutlet UILabel* lblAlert;

@property BOOL loadedAd;

@end
