//
//  AppDelegate.h
//  CarPrice
//
//  Created by Trung 1988 on 2/2/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <UserNotifications/UserNotifications.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import "Appirater.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, FBInterstitialAdDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate, GADInterstitialDelegate>

@property (nonnull, nonatomic) UIWindow *window;

@property (nonnull, retain) FBInterstitialAd *fullScreenAd;

@property (nullable, retain) UIViewController* targetView;
@property (nonnull) SEL pendingSelector;

@property (nonnull, retain) GADInterstitial* interstitial;

-(BOOL) showAd:(nullable SEL) pendingTask
        target:(nullable id) target;

@property (nonnull, retain) UILocalNotification* localNotif;

@end

