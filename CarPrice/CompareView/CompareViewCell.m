//
//  CompareViewCell.m
//  CarPrice
//
//  Created by Trung 1988 on 2/8/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "CompareViewCell.h"

@implementation CompareViewCell

@synthesize lblTitle, lblContent1, lblContent2;

@synthesize dicCar1, dicCar2;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) drawCell:(NSIndexPath*) indexPath{
    
    lblContent1.textColor = [UIColor hexColor:@"#686868"];
    lblContent2.textColor = [UIColor hexColor:@"#686868"];
    
    if (indexPath.row == 0) {
        lblTitle.text = @"Tên xe";
        lblContent1.text = [dicCar1 objectForKey:@"name"];
        lblContent2.text = [dicCar2 objectForKey:@"name"];
    }
    if (indexPath.row == 1) {
        lblTitle.text = @"Giá xe";
        lblContent1.text = [dicCar1 objectForKey:@"price"];
        lblContent2.text = [dicCar2 objectForKey:@"price"];
        
        NSInteger price1 = [[[dicCar1 objectForKey:@"price"] stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue];
        NSInteger price2 = [[[dicCar2 objectForKey:@"price"] stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue];
        
        if(price1 > price2)
            lblContent2.textColor = [UIColor hexColor:@"#CD5C5C"];
        else
            lblContent1.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    if (indexPath.row == 2) {
        lblTitle.text = @"Kích thước (mm)";
        lblContent1.text = [dicCar1 objectForKey:@"size"];
        lblContent2.text = [dicCar2 objectForKey:@"size"];
    }
    if (indexPath.row == 3) {
        lblTitle.text = @"Loại xe";
        lblContent1.text = [dicCar1 objectForKey:@"type"];
        lblContent2.text = [dicCar2 objectForKey:@"type"];
    }
    if (indexPath.row == 4) {
        lblTitle.text = @"Động cơ";
        lblContent1.text = [dicCar1 objectForKey:@"engine"];
        lblContent2.text = [dicCar2 objectForKey:@"engine"];
    }
    if (indexPath.row == 5) {
        lblTitle.text = @"Hộp số";
        lblContent1.text = [dicCar1 objectForKey:@"gear"];
        lblContent2.text = [dicCar2 objectForKey:@"gear"];
    }
    if (indexPath.row == 6) {
        lblTitle.text = @"Công suất (mã lực)";
        lblContent1.text = [dicCar1 objectForKey:@"power"];
        lblContent2.text = [dicCar2 objectForKey:@"power"];
        
        NSInteger power1 = [[dicCar1 objectForKey:@"power"] integerValue];
        NSInteger power2 = [[dicCar2 objectForKey:@"power"] integerValue];
        if(power1 > power2)
            lblContent1.textColor = [UIColor hexColor:@"#CD5C5C"];
        else
            lblContent2.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    if (indexPath.row == 7) {
        lblTitle.text = @"Momen xoắn (Nm)";
        lblContent1.text = [dicCar1 objectForKey:@"momen"];
        lblContent2.text = [dicCar2 objectForKey:@"momen"];
        
        NSInteger momen1 = [[dicCar1 objectForKey:@"momen"] integerValue];
        NSInteger momen2 = [[dicCar2 objectForKey:@"momen"] integerValue];
        if(momen1 > momen2)
            lblContent1.textColor = [UIColor hexColor:@"#CD5C5C"];
        else
            lblContent2.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    if (indexPath.row == 8) {
        lblTitle.text = @"Nguồn gốc";
        lblContent1.text = [dicCar1 objectForKey:@"origin"];
        lblContent2.text = [dicCar2 objectForKey:@"origin"];
    }
    if (indexPath.row == 9) {
        lblTitle.text = @"Khoảng sáng gầm(mm)";
        lblContent1.text = [dicCar1 objectForKey:@"ground"];
        lblContent2.text = [dicCar2 objectForKey:@"ground"];
        
        NSInteger ground1 = [[dicCar1 objectForKey:@"ground"] integerValue];
        NSInteger ground2 = [[dicCar2 objectForKey:@"ground"] integerValue];
        if(ground1 > ground2)
            lblContent1.textColor = [UIColor hexColor:@"#CD5C5C"];
        else
            lblContent2.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
    if (indexPath.row == 10) {
        lblTitle.text = @"Bình xăng (lít)";
        lblContent1.text = [dicCar1 objectForKey:@"gasoline"];
        lblContent2.text = [dicCar2 objectForKey:@"gasoline"];
        
        NSInteger gasoline1 = [[dicCar1 objectForKey:@"gasoline"] integerValue];
        NSInteger gasoline2 = [[dicCar2 objectForKey:@"gasoline"] integerValue];
        if(gasoline1 > gasoline2)
            lblContent1.textColor = [UIColor hexColor:@"#CD5C5C"];
        else
            lblContent2.textColor = [UIColor hexColor:@"#CD5C5C"];
    }
}

@end
