//
//  CompareViewCell.h
//  CarPrice
//
//  Created by Trung 1988 on 2/8/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompareViewCell : UITableViewCell

@property (nonnull, retain) IBOutlet UILabel* lblTitle;
@property (nonnull, retain) IBOutlet UILabel* lblContent1;
@property (nonnull, retain) IBOutlet UILabel* lblContent2;

@property (nonnull, retain) NSDictionary* dicCar1;
@property (nonnull, retain) NSDictionary* dicCar2;

-(void) drawCell:(nullable NSIndexPath*) indexPath;

@end
