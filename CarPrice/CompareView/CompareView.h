//
//  CompareView.h
//  CarPrice
//
//  Created by Trung 1988 on 2/8/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompareViewCell.h"

@interface CompareView : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource>

@property (nonnull, retain) NSDictionary* dicDatabase;
@property (nonnull, retain) IBOutlet UIView* viewMenu;

@property (nonnull, retain) IBOutlet UIPickerView* pickContent;
@property (nonnull, retain) IBOutlet NSLayoutConstraint* pickContentBottom;

@property (nonnull, retain) IBOutlet UITextField* txtBrand1;
@property (nonnull, retain) IBOutlet UITextField* txtType1;
@property (nonnull, retain) IBOutlet UITextField* txtBrand2;
@property (nonnull, retain) IBOutlet UITextField* txtType2;

@property (nonnull, retain) UITextField* currentTextField;

@property (nonnull, retain) NSArray* arrBrand;

-(void) loadContent;

@property (nonnull, retain) NSDictionary* dicCar1;
@property (nonnull, retain) NSDictionary* dicCar2;

@property (nonnull, retain) IBOutlet UITableView* tblCompare;
@property (nonnull, retain) IBOutlet UIView* headerView;
@property (nonnull, retain) IBOutlet UIImageView* headerImage1;
@property (nonnull, retain) IBOutlet UIImageView* headerImage2;

@end
