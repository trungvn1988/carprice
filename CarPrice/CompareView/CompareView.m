//
//  CompareView.m
//  CarPrice
//
//  Created by Trung 1988 on 2/8/17.
//  Copyright © 2017 BlueSun. All rights reserved.
//

#import "CompareView.h"

@interface CompareView ()

@end

@implementation CompareView

@synthesize dicDatabase, arrBrand;
@synthesize viewMenu;

@synthesize txtType1, txtType2, txtBrand1, txtBrand2;
@synthesize pickContent, pickContentBottom;
@synthesize currentTextField;
@synthesize headerView;

@synthesize dicCar1, dicCar2, tblCompare;
@synthesize headerImage1, headerImage2;

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arrBrand.count;
}

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [arrBrand objectAtIndex:row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    currentTextField.text = [arrBrand objectAtIndex:row];
    if([currentTextField isEqual:txtBrand1]){
        NSDictionary* dicListCar = [[dicDatabase objectForKey:txtBrand1.text] objectForKey:@"car"];
        NSArray *keys = [dicListCar allKeys];
        NSArray* arrCar = [keys sortedArrayUsingComparator:^(id a, id b) {
            return [a compare:b options:NSNumericSearch];
        }];
        txtType1.text = [arrCar firstObject];
    }
    if([currentTextField isEqual:txtBrand2]){
        NSDictionary* dicListCar = [[dicDatabase objectForKey:txtBrand2.text] objectForKey:@"car"];
        NSArray *keys = [dicListCar allKeys];
        NSArray* arrCar = [keys sortedArrayUsingComparator:^(id a, id b) {
            return [a compare:b options:NSNumericSearch];
        }];
        txtType2.text = [arrCar firstObject];
    }
    
    [self loadContent];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"So Sánh";
    [self setColorTitle:[UIColor whiteColor]];
    
    dicDatabase = [self getFileWithKey:@"DATABASE"];
    
    pickContentBottom.constant = - pickContent.height;
    
    UITapGestureRecognizer* tapHide = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePickerView)];
    [self.view addGestureRecognizer:tapHide];
    
    NSArray *keys = [dicDatabase allKeys];
    arrBrand = [keys sortedArrayUsingComparator:^(id a, id b) {
        return [a compare:b options:NSNumericSearch];
    }];
    
    txtBrand1.text = [arrBrand firstObject];
    txtBrand2.text = [arrBrand firstObject];
    
    [tblCompare registerNib:[UINib nibWithNibName:@"CompareViewCell" bundle:nil] forCellReuseIdentifier:@"CompareViewCell"];
    
    [self loadContent];
    
    tblCompare.tableFooterView = [UIView new];
    tblCompare.tableHeaderView = headerView;
    
    [(AppDelegate*)[UIApplication sharedApplication].delegate showAd:nil target:self];
}

-(void) loadContent{
    dicCar1 = [[[dicDatabase objectForKey:txtBrand1.text] objectForKey:@"car"] objectForKey:txtType1.text];
    dicCar2 = [[[dicDatabase objectForKey:txtBrand2.text] objectForKey:@"car"] objectForKey:txtType2.text];
    
    [tblCompare reloadData];
    
    NSString* imgURL1 = [[dicCar1 objectForKey:@"image"] objectForKey:@"main"];
    NSString* imgURL2 = [[dicCar2 objectForKey:@"image"] objectForKey:@"main"];
    
    [headerImage1 sd_setImageWithURL:[NSURL URLWithString:imgURL1]
                    placeholderImage:[UIImage imageNamed:@"no-image"]];
    [headerImage2 sd_setImageWithURL:[NSURL URLWithString:imgURL2]
                    placeholderImage:[UIImage imageNamed:@"no-image"]];
    
    NSLog(@"dicCar1 %@", dicCar1);
}

-(void) hidePickerView{
    pickContentBottom.constant = - pickContent.height;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    currentTextField.backgroundColor = [UIColor whiteColor];
    currentTextField.textColor = [UIColor hexColor:@"#686868"];
    if(currentTextField.placeholder)
        currentTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:currentTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor] }];
    currentTextField = nil;
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    currentTextField.backgroundColor = [UIColor whiteColor];
    currentTextField.textColor = [UIColor hexColor:@"#686868"];
    currentTextField = nil;
    
    if([textField isEqual:txtBrand1] || [textField isEqual:txtBrand2]){
        NSArray *keys = [dicDatabase allKeys];
        arrBrand = [keys sortedArrayUsingComparator:^(id a, id b) {
            return [a compare:b options:NSNumericSearch];
        }];
    }
    else if([textField isEqual:txtType1]){
        NSDictionary* dicListCar = [[dicDatabase objectForKey:txtBrand1.text] objectForKey:@"car"];
        NSArray *keys = [dicListCar allKeys];
        arrBrand = [keys sortedArrayUsingComparator:^(id a, id b) {
            return [a compare:b options:NSNumericSearch];
        }];
    }
    else if([textField isEqual:txtType2]){
        NSDictionary* dicListCar = [[dicDatabase objectForKey:txtBrand2.text] objectForKey:@"car"];
        NSArray *keys = [dicListCar allKeys];
        arrBrand = [keys sortedArrayUsingComparator:^(id a, id b) {
            return [a compare:b options:NSNumericSearch];
        }];
    }
    
    currentTextField = textField;
    currentTextField.backgroundColor = [UIColor hexColor:@"#686868"];
    currentTextField.textColor = [UIColor whiteColor];
    currentTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:currentTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    
    [pickContent reloadAllComponents];
    [pickContent selectRow:0 inComponent:0 animated:NO];
    
    pickContentBottom.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    return NO;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 11;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"CompareViewCell";
    
    CompareViewCell *cell = [tblCompare dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (!cell) {
        cell = [CompareViewCell new];
    }
    
    cell.dicCar1 = dicCar1;
    cell.dicCar2 = dicCar2;
    
    [cell drawCell:indexPath];
    
    return cell;
}

@end
